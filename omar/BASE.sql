CREATE TABLE grupos (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, descripcion VARCHAR(100) NOT NULL UNIQUE, observaciones TEXT, estatus INT(1)) Engine = InnoDB;
CREATE TABLE secciones (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, descripcion VARCHAR(100) NOT NULL UNIQUE, observaciones TEXT, estatus INT(1)) Engine = InnoDB;
