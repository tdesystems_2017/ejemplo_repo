from flask import Flask
from flask_restful import Api
app = Flask(__name__)

from recursos.paises import Paises
from recursos.consolas import Consolas
from recursos.grupo import Grupo
from recursos.seccion import Seccion
from recursos.accion import Accion
from recursos.accion_grupo import AccionGrupo
from recursos.cliente import Cliente
from recursos.personal import Personal
from recursos.personal_proyecto import PersonalProyecto
from recursos.proyecto import Proyecto
from recursos.token import Token
from recursos.usuario import Usuario

api = Api(app)

api.add_resource(Paises, '/paises', '/paises/<string:pais>')
api.add_resource(Consolas, '/consolas/<string:consola>', '/consolas/<string:consola>/<string:modelo>')
api.add_resource(Grupo, '/grupos', '/grupos/<string:descripcion>')
api.add_resource(Seccion, '/secciones', '/secciones/<string:descripcion>')
api.add_resource(Accion, '/acciones', '/acciones/<string:descripcion>')
api.add_resource(AccionGrupo, '/accion_grupo', '/accion_grupo/<string:descripcion>')
api.add_resource(Cliente, '/clientes', '/clientes/<string:descripcion>')
api.add_resource(Personal, '/personal', '/personal/<string:descripcion>')
api.add_resource(PersonalProyecto, '/personal_proyecto', '/personal_proyecto/<string:descripcion>')
api.add_resource(Proyecto, '/proyectos', '/proyectos/<string:descripcion>')
api.add_resource(Token, '/tokens', '/tokens/<string:descripcion>')
api.add_resource(Usuario, '/usuarios', '/usuarios/<string:descripcion>')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5000)
