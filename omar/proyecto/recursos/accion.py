from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json


client = MongoClient()
db = client.sistema

app = Flask(__name__)


class Accion(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('seccion_id', required=True)
        args = parser.parse_args()
        descripcion = args['descripcion']
        seccion_id = args['seccion_id']
        db.accion.insert({'descripcion': descripcion, 'seccion_id': seccion_id})
        return {'mensaje': 'post', 'descripcion': descripcion, 'seccion_id': seccion_id}

    def get(self, descripcion):
        accion = db.accion.find()
        acciones_enviar = []
        for accion in accion:
            accion_json = {}
            accion_json['descripcion'] = accion['descripcion']
            accion_json['seccion_id'] = accion['seccion_id']
            accion_json['_id'] = str(accion['_id'])
            acciones_enviar.append(accion_json)
        return {'mensaje': 'get', 'accion': acciones_enviar}

    def put(self, descripcion):
        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('seccion_id', required=True)
        args = parser.parse_args()
        descripcion_update = args['descripcion']
        seccion_id_update = args['seccion_id']
        db.accion.update({'_id': ObjectId(descripcion)}, {'$set': {'descripcion': descripcion_update, 'seccion_id': seccion_id_update}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, descripcion):
        db.accion.remove({'_id': ObjectId(descripcion)})
        return {'mensaje': 'delete'}
