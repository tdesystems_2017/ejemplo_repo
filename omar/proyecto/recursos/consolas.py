from flask_restful import Resource, reqparse


class Consolas(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('consola', required=True)
        args = parser.parse_args()
        consola = args['consola']
        descripcion, ultimo_modelo = obtener_info(consola)
        return {'mensaje': 'post', 'consola': consola, 'descripcion': descripcion, 'ultimo_modelo': ultimo_modelo}

    #def get(self, consola):
    #    descripcion, ultimo_modelo = obtener_info(consola)
    #    return {'mensaje': 'get', 'consola': consola, 'descripcion': descripcion, 'ultimo_modelo': ultimo_modelo}

    def get(self, consola, modelo):
        descripcion, existe = obtener_info_modelo(consola,modelo)
        return {'mensaje': 'get', 'consola': consola, 'descripcion': descripcion, 'existe': existe}

    def put(self):
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self):
        return {'mensaje': 'delete'}


def obtener_info(consola):
    if consola == 'playstation':
            descripcion = 'consola perteneciente a la compania Sony'
            ultimo_modelo = 'Playstation 4 Pro'
    elif consola == 'xbox':
            descripcion = 'consola perteneciente a la compania Microsoft'
            ultimo_modelo = 'Xbox One Scorpio'
    elif consola == 'wii' or consola == 'switch':
            descripcion = 'consola perteneciente a la compania Nintendo'
            ultimo_modelo = 'Switch'
    else:
            descripcion = 'consola no encontrada'
            ultimo_modelo = 'Sin informacion'
    return descripcion, ultimo_modelo


def obtener_info_modelo(consola, modelo):
    if consola == 'playstation':
        if modelo == 'ps4':
            existe = True
            descripcion = 'cosola de 4ta generacion'
        elif modelo == 'ps3':
            existe = True
            descripcion = 'cosola de 3ra generacion'
        elif modelo == 'ps2':
            existe = True
            descripcion = 'cosola de 2da generacion'
        else:
            existe = False
            descripcion = 'no encontrada'
    elif consola == 'xbox':
            if modelo == 'one':
                existe = True
                descripcion = 'cosola de 4ta generacion'
            elif modelo == '360':
                existe = True
                descripcion = 'cosola de 3ra generacion'
            elif modelo == 'xbox':
                existe = True
                descripcion = 'cosola de 2da generacion'
            else:
                existe = False
                descripcion = 'no encontrada'
    elif consola == 'wii' or consola == 'switch':
        if modelo == 'switch':
            existe = True
            descripcion = 'cosola de 4ta generacion'
        elif modelo == 'wiiu':
            existe = True
            descripcion = 'cosola de 3ra generacion'
        elif modelo == 'wii':
            existe = True
            descripcion = 'cosola de 2da generacion'
        else:
            existe = False
            descripcion = 'no encontrada'
    else:
        descripcion = 'consola no encontrada'
        existe = False
    return descripcion, existe
