from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json


client = MongoClient()
db = client.sistema

app = Flask(__name__)


class Token(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('grupo', required=True)
        parser.add_argument('usuario', required=True)
        parser.add_argument('fecha_hora', required=True)
        args = parser.parse_args()
        grupo = args['grupo']
        usuario = args['usuario']
        fecha_hora = args['fecha_hora']
        db.token.insert({'grupo': grupo, 'usuario': usuario, 'fecha_hora': fecha_hora})
        return {'mensaje': 'post', 'grupo': grupo, 'usuario': usuario, 'fecha_hora': fecha_hora}

    def get(self, descripcion):
        tokens = db.token.find()
        tokens_enviar = []
        for token in tokens:
            token_json = {}
            token_json['grupo'] = token['grupo']
            token_json['usuario'] = token['usuario']
            token_json['fecha_hora'] = token['fecha_hora']
            token_json['_id'] = str(token['_id'])
            tokens_enviar.append(token_json)
        return {'mensaje': 'get', 'token': tokens_enviar}

    def put(self, descripcion):
        parser = reqparse.RequestParser()
        parser.add_argument('grupo', required=True)
        parser.add_argument('usuario', required=True)
        parser.add_argument('fecha_hora', required=True)
        args = parser.parse_args()
        grupo_update = args['grupo']
        usuario_update = args['usuario']
        fecha_hora_update = args['fecha_hora']
        db.token.update({'_id': ObjectId(descripcion)}, {'$set': {'grupo': grupo_update, 'usuario': usuario_update, 'fecha_hora': fecha_hora_update}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, grupo):
        db.token.remove({'_id': ObjectId(grupo)})
        return {'mensaje': 'delete'}
