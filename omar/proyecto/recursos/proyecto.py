from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json


client = MongoClient()
db = client.sistema

app = Flask(__name__)


class Proyecto(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('nombre', required=True)
        parser.add_argument('tiempo_estimado', required=True)
        parser.add_argument('costo', required=True)
        parser.add_argument('cliente_id', required=True)
        args = parser.parse_args()
        nombre = args['nombre']
        tiempo_estimado = args['tiempo_estimado']
        costo = args['costo']
        cliente_id = args['cliente_id']
        db.proyecto.insert({'nombre': nombre, 'tiempo_estimado': tiempo_estimado, 'costo': costo, 'cliente_id': cliente_id})
        return {'mensaje': 'post', 'nombre': nombre, 'tiempo_estimado': tiempo_estimado, 'costo': costo, 'cliente_id': cliente_id}

    def get(self, descripcion):
        proyectos = db.proyecto.find()
        proyectos_enviar = []
        for proyecto in proyectos:
            proyecto_json = {}
            proyecto_json['nombre'] = proyecto['nombre']
            proyecto_json['tiempo_estimado'] = proyecto['tiempo_estimado']
            proyecto_json['costo'] = proyecto['costo']
            proyecto_json['cliente_id'] = proyecto['cliente_id']
            proyecto_json['_id'] = str(proyecto['_id'])
            proyectos_enviar.append(proyecto_json)
        return {'mensaje': 'get', 'proyecto': proyectos_enviar}

    def put(self, nombre):
        parser = reqparse.RequestParser()
        parser.add_argument('nombre', required=True)
        parser.add_argument('tiempo_estimado', required=True)
        parser.add_argument('costo', required=True)
        parser.add_argument('cliente_id', required=True)
        args = parser.parse_args()
        nombre_update = args['nombre']
        tiempo_estimado_update = args['tiempo_estimado']
        costo_update = args['costo']
        cliente_id = args['cliente_id']
        db.proyecto.update({'_id': ObjectId(nombre)}, {'$set': {'nombre': nombre_update, 'tiempo_estimado': tiempo_estimado_update, 'costo': costo_update, 'cliente_id': cliente_id}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, nombre):
        db.proyecto.remove({'_id': ObjectId(nombre)})
        return {'mensaje': 'delete'}
