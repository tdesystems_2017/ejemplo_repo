from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json


client = MongoClient()
db = client.sistema

app = Flask(__name__)


class AccionGrupo(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('accion_id', required=True)
        parser.add_argument('grupo_id', required=True)
        args = parser.parse_args()
        accion_id = args['accion_id']
        grupo_id = args['grupo_id']
        db.accion_grupo.insert({'accion_id': accion_id, 'grupo_id': grupo_id})
        return {'mensaje': 'post', 'accion_id': accion_id, 'grupo_id': grupo_id}

    def get(self, descripcion):
        accion_grupos = db.accion_grupo.find()
        accion_grupos_enviar = []
        for accion_grupo in accion_grupos:
            accion_grupo_json = {}
            accion_grupo_json['accion_id'] = accion_grupo['accion_id']
            accion_grupo_json['grupo_id'] = accion_grupo['grupo_id']
            accion_grupo_json['_id'] = str(accion_grupo['_id'])
            accion_grupos_enviar.append(accion_grupo_json)
        return {'mensaje': 'get', 'accion_grupo': accion_grupos_enviar}

    def put(self, descripcion):
        parser = reqparse.RequestParser()
        parser.add_argument('accion_id', required=True)
        parser.add_argument('grupo_id', required=True)
        args = parser.parse_args()
        descripcion_update = args['accion_id']
        observaciones_update = args['grupo_id']
        db.accion_grupo.update({'_id': ObjectId(descripcion)}, {'$set': {'accion_id': descripcion_update, 'grupo_id': observaciones_update}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, accion_id):
        db.accion_grupo.remove({'_id': ObjectId(accion_id)})
        return {'mensaje': 'delete'}
