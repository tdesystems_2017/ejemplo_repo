from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json


client = MongoClient()
db = client.sistema

app = Flask(__name__)


class Seccion(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('observaciones', required=True)
        parser.add_argument('status', required=True)
        args = parser.parse_args()
        descripcion = args['descripcion']
        observaciones = args['observaciones']
        status = args['status']
        db.seccion.insert({'descripcion': descripcion, 'observaciones': observaciones, 'status': status})
        return {'mensaje': 'post', 'descripcion': descripcion, 'observaciones': observaciones, 'status': status}

    def get(self, descripcion):
        secciones = db.seccion.find()
        grupos_enviar = []
        for seccion in secciones:
            grupo_json = {}
            grupo_json['descripcion'] = seccion['descripcion']
            grupo_json['observaciones'] = seccion['observaciones']
            grupo_json['status'] = seccion['status']
            grupo_json['_id'] = str(seccion['_id'])
            grupos_enviar.append(grupo_json)
        return {'mensaje': 'get', 'seccion': grupos_enviar}

    def put(self, descripcion):
        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('observaciones', required=True)
        parser.add_argument('status', required=True)
        args = parser.parse_args()
        descripcion_update = args['descripcion']
        observaciones_update = args['observaciones']
        status_update = args['status']
        db.seccion.update({'_id': ObjectId(descripcion)}, {'$set': {'descripcion': descripcion_update, 'observaciones': observaciones_update, 'status': status_update}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, descripcion):
        db.seccion.remove({'_id': ObjectId(descripcion)})
        return {'mensaje': 'delete'}
