from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json


client = MongoClient()
db = client.sistema

app = Flask(__name__)


class Personal(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('nombre', required=True)
        parser.add_argument('apellido_paterno', required=True)
        parser.add_argument('apellido_materno', required=True)
        parser.add_argument('telefono', required=True)
        parser.add_argument('curp', required=True)
        parser.add_argument('direccion', required=True)
        parser.add_argument('sueldo', required=True)
        parser.add_argument('especialidad', required=True)
        parser.add_argument('correo', required=True)
        args = parser.parse_args()
        nombre = args['nombre']
        apellido_paterno = args['apellido_paterno']
        apellido_materno = args['apellido_materno']
        telefono = args['telefono']
        curp = args['curp']
        direccion = args['direccion']
        sueldo = args['sueldo']
        especialidad = args['especialidad']
        correo = args['correo']
        db.personal.insert({'nombre': nombre, 'apellido_paterno': apellido_paterno, 'apellido_materno': apellido_materno, 'telefono': telefono, 'curp': curp, 'direccion': direccion, 'sueldo': sueldo, 'especialidad': especialidad, 'correo': correo})
        return {'mensaje': 'post', 'nombre': nombre, 'apellido_paterno': apellido_paterno, 'apellido_materno': apellido_materno, 'telefono': telefono, 'curp': curp, 'direccion': direccion, 'sueldo': sueldo, 'especialidad': especialidad, 'correo': correo}

    def get(self, descripcion):
        personales = db.personal.find()
        personales_enviar = []
        for personal in personales:
            personal_json = {}
            personal_json['nombre'] = personal['nombre']
            personal_json['apellido_paterno'] = personal['apellido_paterno']
            personal_json['apellido_materno'] = personal['apellido_materno']
            personal_json['telefono'] = personal['telefono']
            personal_json['curp'] = personal['curp']
            personal_json['direccion'] = personal['direccion']
            personal_json['sueldo'] = personal['sueldo']
            personal_json['especialidad'] = personal['especialidad']
            personal_json['correo'] = personal['correo']
            personal_json['_id'] = str(personal['_id'])
            personales_enviar.append(personal_json)
        return {'mensaje': 'get', 'personal': personales_enviar}

    def put(self, descripcion):
        parser = reqparse.RequestParser()
        parser.add_argument('nombre', required=True)
        parser.add_argument('apellido_paterno', required=True)
        parser.add_argument('apellido_materno', required=True)
        parser.add_argument('telefono', required=True)
        parser.add_argument('curp', required=True)
        parser.add_argument('direccion', required=True)
        parser.add_argument('sueldo', required=True)
        parser.add_argument('especialidad', required=True)
        parser.add_argument('correo', required=True)
        args = parser.parse_args()
        nombre = args['nombre']
        apellido_paterno = args['apellido_paterno']
        apellido_materno = args['apellido_materno']
        telefono = args['telefono']
        curp = args['curp']
        direccion = args['direccion']
        sueldo = args['sueldo']
        especialidad = args['especialidad']
        correo = args['correo']
        db.personal.update({'_id': ObjectId(descripcion)}, {'$set': {'nombre': nombre, 'apellido_paterno': apellido_paterno, 'apellido_materno': apellido_materno, 'telefono': telefono, 'curp': curp, 'direccion': direccion, 'sueldo': sueldo, 'especialidad': especialidad, 'correo': correo}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, descripcion):
        db.personal.remove({'_id': ObjectId(descripcion)})
        return {'mensaje': 'delete'}
