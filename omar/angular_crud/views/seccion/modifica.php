<form class="form-horizontal alert alert-info" id="modificaForm" ng-submit="modificaInfo(modifica,'seccion','modifica_db');" hidden>

	<h3 class="text-center">Modifica</h3>
	<div class="form-group">
		<label for="Descripcion">Descripcion</label>
		<input type="text" name="descripcion" class="form-control" placeholder="Ingresa Descripcion" ng-model="modifica.descripcion" value="{{modifica.descripcion}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.descripcion.$invalid && modifica.descripcion.$dirty">El campo descripcion esta vacio</p>
		
	</div>
	<div class="form-group">
		<label for="Observaciones">Observaciones</label>
		<input type="text" name="observaciones" class="form-control" placeholder="Ingresa Observaciones" ng-model="modifica.observaciones" value="{{modifica.observaciones}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.observaciones.$invalid && modifica.observaciones.$dirty">El campo Observaciones esta vacio</p>
	</div>

	<div class="form-group text-center">
		<label for="grupo_status">Estatus</label>
		<label for="" class="radio-inline status">
			<input type="radio" name="grupo_status" value="0" ng-model="modifica.status">Inactivo
		</label>

		<label for="" class="radio-inline status">
			<input type="radio" name="grupo_status" value="1" ng-model="modifica.status">Activo
		</label>

		
	</div>

	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="modifica.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
	</div>
</form>