<form class="form-horizontal alert alert-info" id="modificaForm" ng-submit="modificaInfo(modifica,'personal','modifica_db');" hidden>

	<h3 class="text-center">Modifica</h3>
	<div class="form-group">
		<label for="Nombre">Nombre</label>
		<input type="text" name="nombre" class="form-control" placeholder="Ingresa Nombre" ng-model="modifica.nombre" value="{{modifica.nombre}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.nombre.$invalid && modifica.nombre.$dirty">El campo nombre esta vacio</p>
	</div>
	<div class="form-group">
		<label for="ApellidoPaterno">Apellido Paterno</label>
		<input type="text" name="apellido_paterno" class="form-control" placeholder="Ingresa Apellido Paterno" ng-model="modifica.apellido_paterno" value="{{modifica.apellido_paterno}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.apellido_paterno.$invalid && modifica.apellido_paterno.$dirty">El campo Apellido Paterno esta vacio</p>
	</div>
	<div class="form-group">
		<label for="ApellidoMaterno">Apellido Materno</label>
		<input type="text" name="apellido_materno" class="form-control" placeholder="Ingresa Apellido Materno" ng-model="modifica.apellido_materno" value="{{modifica.apellido_materno}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.apellido_materno.$invalid && modifica.apellido_materno.$dirty">El campo Apellido Materno esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Telefono">telefono</label>
		<input type="text" name="telefono" class="form-control" placeholder="Ingresa telefono" ng-model="modifica.telefono" value="{{modifica.telefono}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.telefono.$invalid && modifica.telefono.$dirty">El campo telefono esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Curp">curp</label>
		<input type="text" name="curp" class="form-control" placeholder="Ingresa curp" ng-model="modifica.curp" value="{{modifica.curp}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.curp.$invalid && modifica.curp.$dirty">El campo curp esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Direccion">Direccion</label>
		<input type="text" name="direccion" class="form-control" placeholder="Ingresa direccion" ng-model="modifica.direccion" value="{{modifica.direccion}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.direccion.$invalid && modifica.direccion.$dirty">El campo direccion esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Sueldo">Sueldo</label>
		<input type="text" name="sueldo" class="form-control" placeholder="Ingresa sueldo" ng-model="modifica.sueldo" value="{{modifica.sueldo}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.sueldo.$invalid && modifica.sueldo.$dirty">El campo sueldo esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Especialidad">Especialidad</label>
		<input type="text" name="especialidad" class="form-control" placeholder="Ingresa especialidad" ng-model="modifica.especialidad" value="{{modifica.especialidad}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.especialidad.$invalid && modifica.especialidad.$dirty">El campo especialidad esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Correo">Correo</label>
		<input type="text" name="correo" class="form-control" placeholder="Ingresa correo" ng-model="modifica.correo" value="{{modifica.correo}}" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="modifica.correo.$invalid && modifica.correo.$dirty">El campo correo esta vacio</p>
	</div>
	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="modifica.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
	</div>
</form>