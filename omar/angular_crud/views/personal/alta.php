<form class="form-horizontal alert alert-warning" name="grupoList" id="altaForm" ng-submit="alta(grupoInfo,'personal','alta_db');" hidden>

	<h3 class="text-center">Alta / Personal</h3>
	<div class="form-group">
		<label for="Nombre">Nombre</label>
		<input type="text" name="nombre" class="form-control" placeholder="Ingresa Nombre" ng-model="grupoInfo.nombre" value="" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.nombre.$invalid && grupoList.nombre.$dirty">El campo nombre esta vacio</p>
		
	</div>
	<div class="form-group">
		<label for="ApellidoPaterno">Apellido Paterno</label>
		<input type="text" name="apellido_paterno" class="form-control" placeholder="Ingresa Apellido Paterno" ng-model="grupoInfo.apellido_paterno" value="" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.apellido_paterno.$invalid && grupoList.apellido_paterno.$dirty">El campo Apellido Paterno esta vacio</p>
	</div>
	<div class="form-group">
		<label for="ApellidoMaterno">Apellido Materno</label>
		<input type="text" name="apellido_materno" class="form-control" placeholder="Ingresa Apellido Materno" ng-model="grupoInfo.apellido_materno" value="" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.apellido_materno.$invalid && grupoList.apellido_materno.$dirty">El campo Apellido Materno esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Telefono">telefono</label>
		<input type="text" name="telefono" class="form-control" placeholder="Ingresa telefono" ng-model="grupoInfo.telefono" value="" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.telefono.$invalid && grupoList.telefono.$dirty">El campo telefono esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Curp">curp</label>
		<input type="text" name="curp" class="form-control" placeholder="Ingresa curp" ng-model="grupoInfo.curp" value="" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.curp.$invalid && grupoList.curp.$dirty">El campo curp esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Direccion">Direccion</label>
		<input type="text" name="direccion" class="form-control" placeholder="Ingresa direccion" ng-model="grupoInfo.direccion" value="" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.direccion.$invalid && grupoList.direccion.$dirty">El campo direccion esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Sueldo">Sueldo</label>
		<input type="text" name="sueldo" class="form-control" placeholder="Ingresa sueldo" ng-model="grupoInfo.sueldo" value="" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.sueldo.$invalid && grupoList.sueldo.$dirty">El campo sueldo esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Especialidad">Especialidad</label>
		<input type="text" name="especialidad" class="form-control" placeholder="Ingresa especialidad" ng-model="grupoInfo.especialidad" value="" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.especialidad.$invalid && grupoList.especialidad.$dirty">El campo especialidad esta vacio</p>
	</div>
	<div class="form-group">
		<label for="Correo">Correo</label>
		<input type="text" name="correo" class="form-control" placeholder="Ingresa correo" ng-model="grupoInfo.correo" value="" autofocus required>	
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.correo.$invalid && grupoList.correo.$dirty">El campo correo esta vacio</p>
	</div>
	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="grupoList.$invalid">Enviar
			
		</button>
	</div>
</form>