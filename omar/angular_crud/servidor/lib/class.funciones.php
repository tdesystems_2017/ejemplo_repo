<?php
require_once('./config/conexion.php');

class funciones{
	public function valida_array_registro($registro){
		if(is_array($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos();
			return $conexion->link;
		}else{
			return False;
		}	
	}
	public function valida_cadena_vacia($cadena){
		if(!empty($cadena)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos();
			return $conexion->link;
		}else{
			return False;
		}
	}
	public function ejecuta_query($query,$link){
		$link->query($query);
		if($link->error){
			return false;
		}else{
			return true;
		}
	}
}
?>