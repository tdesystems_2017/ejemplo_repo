<?php
require_once('./lib/nusoap.php');

if (!isset($_GET['seccion'])) {
	$seccion='';
}else{
    $seccion = $_GET['seccion'];
    $cliente=new nusoap_client('http://localhost/ejemplo_repo/omar/angular_crud/servidor/server.php?wsdl','wsdl');
}
if (!isset($_GET['accion'])) {
    $accion='';
}else{
    $accion = $_GET['accion'];
}
if (empty($accion) || empty($seccion)) {
    header("Location: http://localhost/ejemplo_repo/omar/angular_crud");
}
if ($accion == 'lista') {
    $resultado =$cliente->call($accion,array('tabla'=>$seccion));
    echo json_encode($resultado);
}
if ($accion == 'alta_db') {
    $data = json_decode(file_get_contents("php://input"));
    if($seccion == 'grupo' || $seccion == 'seccion'){
        $descripcion = $data->descripcion;
        $observaciones = $data->observaciones;
        $status = $data->status;
        $array = array('descripcion'=>$descripcion,"observaciones"=>$observaciones,"status"=>$status);
    }else if($seccion == 'accion'){
        $descripcion = $data->descripcion;
        $seccion_id = $data->seccion_id;
        $array = array('descripcion' => $descripcion, 'seccion_id' => $seccion_id);
    }else if($seccion == 'accion_grupo'){
        $accion_id = $data->accion_id;
        $grupo_id = $data->grupo_id;
        $array = array('accion_id' => $accion_id, 'grupo_id' => $grupo_id);
    }else if($seccion == 'personal'){
            $nombre= $data->nombre;
            $apellido_paterno= $data->apellido_paterno;
            $apellido_materno= $data->apellido_materno;
            $telefono= $data->telefono;
            $curp= $data->curp;
            $direccion= $data->direccion;
            $sueldo= $data->sueldo;
            $especialidad= $data->especialidad;
            $correo= $data->correo;
            $array = array(
            'nombre'=>'nombre',
            'apellido_paterno'=>'apellido_paterno',
            'apellido_materno'=>'apellido_materno',
            'telefono'=>'telefono',
            'curp'=>'curp',
            'direccion'=>'direccion',
            'sueldo'=>'sueldo',
            'especialidad'=>'especialidad',
            'correo'=>'correo');
    }
    

    $resultado=$cliente->call($accion,array('registro'=> $array,'tabla'=>$seccion));

    if ($resultado) {
        echo true;
    }
}
if($accion == 'modifica_db'){
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->id;
    if($seccion == 'grupo' || $seccion == 'seccion'){
        $descripcion = $data->descripcion;
        $observaciones = $data->observaciones;
        $status = $data->status;
        $array = array('descripcion'=>$descripcion,"observaciones"=>$observaciones,"status"=>$status);
    }else if($seccion == 'accion'){
        $descripcion = $data->descripcion;
        $seccion_id = $data->seccion_id;
        $array = array('descripcion' => $descripcion, 'seccion_id' => $seccion_id);
    }else if($seccion == 'accion_grupo'){
        $accion_id = $data->accion_id;
        $grupo_id = $data->grupo_id;
        $array = array('accion_id' => $accion_id, 'grupo_id' => $grupo_id);
    }else if($seccion == 'personal'){
            $nombre= $data->nombre;
            $apellido_paterno= $data->apellido_paterno;
            $apellido_materno= $data->apellido_materno;
            $telefono= $data->telefono;
            $curp= $data->curp;
            $direccion= $data->direccion;
            $sueldo= $data->sueldo;
            $especialidad= $data->especialidad;
            $correo= $data->correo;
            $array = array(
            'nombre'=>'nombre',
            'apellido_paterno'=>'apellido_paterno',
            'apellido_materno'=>'apellido_materno',
            'telefono'=>'telefono',
            'curp'=>'curp',
            'direccion'=>'direccion',
            'sueldo'=>'sueldo',
            'especialidad'=>'especialidad',
            'correo'=>'correo');
    }
    
    $resultado = $cliente->call($accion,array('registro' => $array,'id' => $id,'tabla' => $seccion));
    if($resultado){
        echo true;
    }
}
if($accion == 'elimina_db'){
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->id;
    $resultado = $cliente->call($accion,array('registro' => $id, 'tabla' => $seccion));
    if($resultado){
        echo true;
    }
}

?>