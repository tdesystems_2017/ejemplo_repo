<?php
require_once './modelos/accion.php';
require_once './modelos/accion_grupo.php';
//require_once './modelos/cliente.php';
require_once './modelos/grupo.php';
require_once './modelos/personal.php';
require_once './modelos/personal_proyecto.php';
require_once './modelos/proyecto.php';
require_once './modelos/seccion.php';
require_once './modelos/usuario.php';

function alta_db($registro=false,$tabla=false){
	$ruta = 'modelo_'.$tabla.'';
	$modelo = new $ruta();
	$registro_id = $modelo->alta_db($registro,$tabla);
	return $registro_id;
}
function elimina_db($registro=False,$tabla=False){
	$ruta = 'modelo_'.$tabla.'';
	$modelo = new $ruta();
	$registro_id=$modelo->elimina_db($registro,$tabla);
	return $registro_id;
}
function modifica_db($registro=False, $id=False,$tabla=False){
	$ruta = 'modelo_'.$tabla.'';
	$modelo = new $ruta();
	$registro_id=$modelo->modifica_db($registro,$id,$tabla);
	return $registro_id;
}
function obten_por_id($id=False,$tabla=False){
	$ruta = 'modelo_'.$tabla.'';
	$modelo = new $ruta();
	$registro_id=$modelo->obten_por_id($id,$tabla);	
	return $registro_id;
}
function lista($tabla=False){
	$ruta = 'modelo_'.$tabla.'';
	$modelo = new $ruta();
	$registro_id=$modelo->lista($tabla);
	return $registro_id;
}
?>