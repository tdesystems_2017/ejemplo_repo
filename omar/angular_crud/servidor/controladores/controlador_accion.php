<?php
require_once './modelos/accion.php';

function alta_db($registro=false,$tabla=false){
	$modelo_accion = new modelo_accion();
	$registro_id = $modelo_accion->alta_db($registro,$tabla);
	return $registro_id;
}
function elimina_db($registro=False,$tabla=False){
	$modelo_accion = new modelo_accion();
	$registro_id=$modelo_accion->elimina_db($registro,$tabla);
	return $registro_id;
}
function modifica_db($registro=False, $id=False,$tabla=False){
	$modelo_accion = new modelo_accion();
	$registro_id=$modelo_accion->modifica_db($registro,$id,$tabla);
	return $registro_id;
}
function obten_por_id($id=False,$tabla=False){
	$modelo_accion = new modelo_accion();
	$registro_id=$modelo_accion->obten_por_id($id,$tabla);	
	return $registro_id;
}
function lista($tabla=False){
	$modelo_accion = new modelo_accion();
	$registro_id=$modelo_accion->lista($tabla);
	return $registro_id;
}
?>