<?php
require_once './modelos/proyecto.php';

function alta_db($registro=false,$tabla=false){
	$modelo_proyecto = new modelo_proyecto();
	$registro_id = $modelo_proyecto->alta_db($registro,$tabla);
	return $registro_id;
}
function elimina_db($registro=False,$tabla=False){
	$modelo_proyecto = new modelo_proyecto();
	$registro_id=$modelo_proyecto->elimina_db($registro,$tabla);
	return $registro_id;
}
function modifica_db($registro=False, $id=False,$tabla=False){
	$modelo_proyecto = new modelo_proyecto();
	$registro_id=$modelo_proyecto->modifica_db($registro,$id,$tabla);
	return $registro_id;
}
function obten_por_id($id=False,$tabla=False){
	$modelo_proyecto = new modelo_proyecto();
	$registro_id=$modelo_proyecto->obten_por_id($id,$tabla);	
	return $registro_id;
}
function lista($tabla=False){
	$modelo_proyecto = new modelo_proyecto();
	$registro_id=$modelo_proyecto->lista($tabla);
	return $registro_id;
}
?>