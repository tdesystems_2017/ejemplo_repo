<?php
require_once './modelos/seccion.php';

function alta_db($registro=false,$tabla=false){
	$modelo_seccion = new modelo_seccion();
	$registro_id = $modelo_seccion->alta_db($registro,$tabla);
	return $registro_id;
}
function elimina_db($registro=False,$tabla=False){
	$modelo_seccion = new modelo_seccion();
	$registro_id=$modelo_seccion->elimina_db($registro,$tabla);
	return $registro_id;
}
function modifica_db($registro=False, $id=False,$tabla=False){
	$modelo_seccion = new modelo_seccion();
	$registro_id=$modelo_seccion->modifica_db($registro,$id,$tabla);
	return $registro_id;
}
function obten_por_id($id=False,$tabla=False){
	$modelo_seccion = new modelo_seccion();
	$registro_id=$modelo_seccion->obten_por_id($id,$tabla);	
	return $registro_id;
}
function lista($tabla=False){
	$modelo_seccion = new modelo_seccion();
	$registro_id=$modelo_seccion->lista($tabla);
	return $registro_id;
}
?>