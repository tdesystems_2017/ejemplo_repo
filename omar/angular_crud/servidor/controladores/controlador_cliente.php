<?php
require_once './modelos/cliente.php';

function alta_db($registro=false,$tabla=false){
	$modelo_cliente = new modelo_cliente();
	$registro_id = $modelo_cliente->alta_db($registro,$tabla);
	return $registro_id;
}
function elimina_db($registro=False,$tabla=False){
	$modelo_cliente = new modelo_cliente();
	$registro_id=$modelo_cliente->elimina_db($registro,$tabla);
	return $registro_id;
}
function modifica_db($registro=False, $id=False,$tabla=False){
	$modelo_cliente = new modelo_cliente();
	$registro_id=$modelo_cliente->modifica_db($registro,$id,$tabla);
	return $registro_id;
}
function obten_por_id($id=False,$tabla=False){
	$modelo_cliente = new modelo_cliente();
	$registro_id=$modelo_cliente->obten_por_id($id,$tabla);	
	return $registro_id;
}
function lista($tabla=False){
	$modelo_cliente = new modelo_cliente();
	$registro_id=$modelo_cliente->lista($tabla);
	return $registro_id;
}
?>