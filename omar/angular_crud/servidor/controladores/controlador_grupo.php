<?php
require_once './modelos/grupo.php';

function alta_db($registro=false,$tabla=false){
	$modelo_grupo = new modelo_grupo();
	$registro_id = $modelo_grupo->alta_db($registro,$tabla);
	return $registro_id;
}
function elimina_db($registro=False,$tabla=False){
	$modelo_grupo = new modelo_grupo();
	$registro_id=$modelo_grupo->elimina_db($registro,$tabla);
	return $registro_id;
}
function modifica_db($registro=False, $id=False,$tabla=False){
	$modelo_grupo = new modelo_grupo();
	$registro_id=$modelo_grupo->modifica_db($registro,$id,$tabla);
	return $registro_id;
}
function obten_por_id($id=False,$tabla=False){
	$modelo_grupo = new modelo_grupo();
	$registro_id=$modelo_grupo->obten_por_id($id,$tabla);	
	return $registro_id;
}
function lista($tabla=False){
	$modelo_grupo = new modelo_grupo();
	$registro_id=$modelo_grupo->lista($tabla);
	return $registro_id;
}
?>