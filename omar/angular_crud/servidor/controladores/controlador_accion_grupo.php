<?php
require_once './modelos/accion_grupo.php';

function alta_db($registro=false,$tabla=false){
	$modelo_accion_grupo = new modelo_accion_grupo();
	$registro_id = $modelo_accion_grupo->alta_db($registro,$tabla);
	return $registro_id;
}
function elimina_db($registro=False,$tabla=False){
	$modelo_accion_grupo = new modelo_accion_grupo();
	$registro_id=$modelo_accion_grupo->elimina_db($registro,$tabla);
	return $registro_id;
}
function modifica_db($registro=False, $id=False,$tabla=False){
	$modelo_accion_grupo = new modelo_accion_grupo();
	$registro_id=$modelo_accion_grupo->modifica_db($registro,$id,$tabla);
	return $registro_id;
}
function obten_por_id($id=False,$tabla=False){
	$modelo_accion_grupo = new modelo_accion_grupo();
	$registro_id=$modelo_accion_grupo->obten_por_id($id,$tabla);	
	return $registro_id;
}
function lista($tabla=False){
	$modelo_accion_grupo = new modelo_accion_grupo();
	$registro_id=$modelo_accion_grupo->lista($tabla);
	return $registro_id;
}
?>