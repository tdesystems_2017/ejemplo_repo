<?php
require_once('./lib/class.funciones.php');

class modelo_accion_grupo{

	 
	public function alta_db($registro=False,$tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_array_registro($registro);
		if(!$link){
			return false;
		}
		$valores = '';
		$campos = '';
		$i=0;
		foreach($registro as $key => $value){
			$campos .= "".$key;
			if(($i+1)!==count($registro)){
				$campos .=",";
			}
			$valores .= "'".$value."'";
			if(($i+1)!==count($registro)){
				$valores .=",";
			}
			$i++;
		}
		$consulta_insercion = "INSERT INTO ".$tabla." (".$campos.") VALUES (".$valores.")";
		if($funciones->ejecuta_query($consulta_insercion,$link)){
			return true;
		}else{
			return false;
		}
	}
	public function elimina_db($registro=False,$tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_cadena_vacia($registro);
		if(!$link){
			return false;
		}
		$consulta_elimina = "DELETE FROM ".$tabla." WHERE id='".$registro."'";
		if($funciones->ejecuta_query($consulta_elimina,$link)){
			return true;
		}else{
			return false;
		}
	}
	public function modifica_db($registro=False, $id=FALSE, $tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_array_registro($registro);
		if(!$link){
			return false;
		}
		$valores = '';
		$i=0;
		foreach ($registro as $key => $value) {
			$valores .= $key."='".$value."'";
			if(($i+1)!==count($registro)){
				$valores .=",";
			}
			$i++;
		}
		$consulta_modifica = "UPDATE ".$tabla." SET ".$valores." WHERE id=".$id;
		if($funciones->ejecuta_query($consulta_modifica,$link)){
			return true;
		}else{
			return false;
		}
	}

	public function obten_por_id($id=False,$tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_cadena_vacia($id);
		if(!$link){
			return false;
		}
		$consulta = "SELECT * FROM ".$tabla." WHERE id =".$id;
		$result =$link->query($consulta);
		if($link->error){
			return false;
		}
		$tabla_base = array(
			'id'=>'id',
			'accion_id'=>'accion_id',
			'grupo_id'=>'grupo_id');
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado[$tabla][$i][$campo]=$row[$campo];
			}
			$i++;
		}
		return $resultado;
	}

	public function lista($tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_cadena_vacia($tabla);
		if(!$link){
			return false;
		}
		$consulta = "SELECT * FROM ".$tabla;
      $result=$link->query($consulta);

         if ($link->error) {

       return false;

      }
		$tabla_base = array(
			'id'=>'id',
			'accion_id'=>'accion_id',
			'grupo_id'=>'grupo_id');

		$i=0;
		$resultado = array();

        while($row=$result->fetch_array()){
        	foreach ($tabla_base as $campo => $valor) {
        		$resultado[$i][$campo]=$row[$campo];
        	}
        	$i++;
        }

       return $resultado;
	}

}

?>