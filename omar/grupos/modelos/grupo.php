<?php
require_once('./lib/class.funciones.php');

class modelo_grupo{

	public $xml_info;
	public function __construct(){
		$this->xml_info=new SimpleXMLElement("<?xml version=\"1.0\"?><empresa></empresa>");
	}
	
	public function alta_db($registro=False,$tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_array_registro($registro);
		if(!$link){
			return false;
		}
		$valores = '';
		$campos = '';
		$i=0;
		foreach($registro as $key => $value){
			$campos .= "".$key;
			if(($i+1)!==count($registro)){
				$campos .=",";
			}
			$valores .= "'".$value."'";
			if(($i+1)!==count($registro)){
				$valores .=",";
			}
			$i++;
		}
		$consulta_insercion = "INSERT INTO ".$tabla." (".$campos.") VALUES (".$valores.")";
		//print_r($consulta_insercion);
		//$link->query($consulta_insercion);
		if($funciones->ejecuta_query($consulta_insercion,$link)){
			$registro_id = $link->insert_id;
			//print_r($registro_id);
			return $this->obten_por_id($registro_id,$tabla);
		}else{
			$resultado=array('total'=>0,$tabla=>array('error'=>"tabla inexistente"));
		$this->array_to_xml($resultado,$this->xml_info);
			return $this->xml_info->asXML();
		}
	}
	public function elimina_db($registro=False,$tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_cadena_vacia($registro);
		if(!$link){
			return false;
		}
		$consulta_elimina = "DELETE FROM ".$tabla." WHERE id='".$registro."'";
		//$link->query($consulta_elimina);
		$resultado=array('total'=>1,$tabla=>array('id'=>$registro));
		$this->array_to_xml($resultado,$this->xml_info);
		$funciones->ejecuta_query($consulta_elimina,$link);
		if(!$funciones->ejecuta_query($consulta_elimina,$link)){
			return false;
		}
		if(!empty($this->xml_info->asXML())){
			return $this->xml_info->asXML();
		}else{
			$resultado=array('total'=>0,$tabla=>array('error'=>"tabla inexistente"));
		$this->array_to_xml($resultado,$this->xml_info);
			return $this->xml_info->asXML();
		}
		//return $funciones->ejecuta_query($consulta_elimina,$link);
	}
	public function modifica_db($registro=False, $id=FALSE, $tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_array_registro($registro);
		if(!$link){
			return false;
		}
		$valores = '';
		/*$campos = Array('descripcion','observaciones','estatus');
		for($i = 0; $i < count($registro); $i++){
			$valores .= $campos[$i]."='".$registro[$i]."'";
			if(($i+1)!==count($registro)){
				$valores .=",";
			}
		}*/
		$i=0;
		foreach ($registro as $key => $value) {
			$valores .= $key."='".$value."'";
			if(($i+1)!==count($registro)){
				$valores .=",";
			}
			$i++;
		}
		$consulta_modifica = "UPDATE ".$tabla." SET ".$valores." WHERE id=".$id;
		if($funciones->ejecuta_query($consulta_modifica,$link)){
			return $this->obten_por_id($id,$tabla);
		}else{
			$resultado=array('total'=>0,$tabla=>array('error'=>"tabla inexistente"));
		$this->array_to_xml($resultado,$this->xml_info);
			return $this->xml_info->asXML();
		}
		//return $funciones->ejecuta_query($consulta_modifica,$link);
	}

	public function obten_por_id($id=False,$tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_cadena_vacia($id);
		if(!$link){
			return false;
		}
		$consulta = "SELECT * FROM ".$tabla." WHERE id =".$id;
		$result =$link->query($consulta);
		if($link->error){
			$resultado=array('total'=>0,$tabla=>array('error'=>"tabla inexistente"));
		$this->array_to_xml($resultado,$this->xml_info);
			return $this->xml_info->asXML();
		}
		$tabla_base = array(
			'id'=>'id',
			'descripcion'=>'descripcion',
			'observaciones'=>'observaciones',
			'estatus'=>'estatus');
		$resultado= array('total'=>1);
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado[$tabla][$i][$campo]=$row[$campo];
			}
			$i++;
			//print_r($i);
		}
		$listado = array('total'=>$i);
		$listado +=$resultado;
		$this->array_to_xml($listado,$this->xml_info);
		if(!empty($this->xml_info->asXML())){
			return $this->xml_info->asXML();
		}else{
			return false;
		}
	}

	public function lista($tabla=False){
		$funciones = new Funciones();
		$link = $funciones->valida_cadena_vacia($tabla);
		if(!$link){
			return false;
		}
		$consulta ="SELECT * FROM ".$tabla;
		$result =$link->query($consulta);
		if($link->error){
			$resultado=array('total'=>0,$tabla=>array('error'=>"tabla inexistente"));
		$this->array_to_xml($resultado,$this->xml_info);
			return $this->xml_info->asXML();
		}
		$tabla_base = array(
			'id'=>'id',
			'descripcion'=>'descripcion',
			'observaciones'=>'observaciones',
			'estatus'=>'estatus');
		$resultado= array('total'=>1);
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado[$tabla][$i][$campo]=$row[$campo];
			}
			$i++;
		}
		$listado = array('total'=>$i);
		$listado +=$resultado;
		$this->array_to_xml($listado,$this->xml_info);
		//return "tiene: ".$this->xml_info.".";
		//print_r("tiene: ".$this->xml_info.".");
		if(!empty($this->xml_info->asXML())){
			return $this->xml_info->asXML();
		}else{
			$resultado=array('total'=>0,$tabla=>array('error'=>"tabla inexistente"));
		$this->array_to_xml($resultado,$this->xml_info);
			return $this->xml_info->asXML();
			//return false;
		}
	}

	public function array_to_xml($array,$xml_info){
		foreach ($array as $key => $value) {
			if(is_array($value)){
				if(!is_numeric($key)){
					$subnodo=$xml_info->addChild("$key");
					$this->array_to_xml($value,$subnodo);
				}else{
					$subnodo = $xml_info->addChild("item$key");
					$this->array_to_xml($value,$subnodo);
				}
			}else{
				$xml_info->addChild("$key",htmlspecialchars("$value"));
			}
		}
	}
}


/*$modelo = new modelo_grupo();
$registro=array('descripcion'=>'insertando7','observaciones'=>'Sin comentarios', 'estatus'=>0);
$resultado = $modelo->alta_db($registro,'grupos');
print_r($resultado);*/
//$modelo = new modelo_grupo();
//$resultado = $modelo->elimina_db(22,'grupos');
//print_r($resultado);
/*$modelo = new modelo_grupo();
$registro=array('descripcion'=>'Modificando 7','observaciones'=>'Observacion','estatus'=>0);
$resultado = $modelo->modifica_db($registro, 14,'grupos');
print_r($resultado);*/
/*$modelo = new modelo_grupo();
$resultado = $modelo->obten_por_id(9,'grupos');
print_r($resultado);
header("Content-type: text/xml");
$modelo = new modelo_grupo();
$resultado = $modelo->lista('gruposa');
print_r($resultado);*/
//header("Content-type: text/xml");
?>