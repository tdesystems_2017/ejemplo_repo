<?php
require_once('./config/conexion.php');

class modelo_seccion{
	
	public function alta_db($registro=False,$tabla=False){
		if(is_array($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos();
			$link = $conexion->link;
		}else{
			return False;
		}
		$valores = '';
		for($i=0;$i<count($registro);$i++){
			$valores .= "'".$registro[$i]."'";
			if(($i+1)!==count($registro)){
				$valores .=",";
			}
		}
		$consulta_insercion = "INSERT INTO ".$tabla." (descripcion,observaciones,estatus) VALUES (".$valores.")";
		//print_r($consulta_insercion);
		$link->query($consulta_insercion);
		if($link->error){
			return $link->error;
		}else{
			$registro_id=$link->insert_id;
			return $registro_id;
		}
	}
}
//$modelo = new modelo_seccion();
//$registro=array('Seccion 886','Observacion 8', 0);
//$resultado = $modelo->alta_db($registro,'secciones');
//print_r($resultado);
?>