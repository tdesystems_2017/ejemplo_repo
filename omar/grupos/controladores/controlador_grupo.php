<?php
require_once './modelos/grupo.php';

function alta_db($registro=false,$tabla=false){
	$modelo_grupo = new modelo_grupo();
	$registro_id = $modelo_grupo->alta_db($registro,$tabla);
	if($registro_id){
		return $registro_id;
	}else{
		return Array('Error'=>'Error al insertar');
	}
}
function elimina_db($registro=False,$tabla=False){
	$modelo_grupo = new modelo_grupo();
	$registro_id=$modelo_grupo->elimina_db($registro,$tabla);
	return $registro_id;
}
function modifica_db($registro=False, $id=False,$tabla=False){
	$modelo_grupo = new modelo_grupo();
	$registro_id=$modelo_grupo->modifica_db($registro,$id,$tabla);
	//if($registro_id){
	return $registro_id;
	/*}else{
		return Array('Error'=>'Error al Modificar');
	}*/
}
function obten_por_id($id=False,$tabla=False){
	$modelo_grupo = new modelo_grupo();
	$registro_id=$modelo_grupo->obten_por_id($id,$tabla);	
	return $registro_id;
}
function lista($tabla=False){
	$modelo_grupo = new modelo_grupo();
	$registro_id=$modelo_grupo->lista($tabla);
	return $registro_id;
}
/*
$arrayName = array('descripcion'=>'insertando 8','observaciones'=>'sin comentarios','estatus'=>1);
print_r(alta_db($arrayName,'grupos'));
print_r(elimina_db(23,'grupos'));
$arrayName = array('descripcion'=>'modificando 8','observaciones'=>'Sin comentarios','estatus'=>1);
print_r(modifica_db($arrayName, 14,'grupos'));
header("Content-type: text/xml");*/
?>