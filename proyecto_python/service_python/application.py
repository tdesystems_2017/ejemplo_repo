from flask import Flask,render_template #Importamos libreria Flask
from flask_restful import Api #Importamos libreria para web services
app = Flask(__name__) #asignacion de la aplicacion
from recursos.paises import Paises
from recursos.grupo import Grupo
from recursos.sesion import Sesion
from recursos.permiso import Permiso

api = Api(app)
api.add_resource(Paises,'/paises')
api.add_resource(Grupo,'/grupo','/grupo/<string:id>','/grupo/token/<string:token>')
api.add_resource(Permiso,'/permiso','/permiso/<string:id>')
api.add_resource(Sesion,'/sesion','/sesion/<string:usuario>/<string:contrasena>','/sesion/<string:token>')

import json

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True,port=5000)
    #Corre nuestro programa por el puerto
