from flask import Flask
from flask_restful import Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId
import time
import json

client = MongoClient()
db = client.sistema

app = Flask(__name__)

class Sesion(Resource):
    def get(self,usuario,contrasena):
        usuarios = db.usuario.find({'usuario':usuario,'contrasena':contrasena})
        #print time.strftime("%c")
        usuario_enviar = []
        for usuario in usuarios:
            usuario_json = {}
            usuario_json['usuario'] = usuario['usuario']
            usuario_json['contrasena'] = usuario['contrasena']
            usuario_json['grupo_id'] = usuario['grupo_id']
            usuario_enviar.append(usuario_json)
        if (len(usuario_enviar) == 0):
            return {'usuario': 'Usuario no encontrado'},401
        else:
            db.token.remove({'usuario':usuario_json['usuario']})
            result = db.token.insert_one({'grupo': usuario_json['grupo_id'], 'usuario': usuario_json['usuario'], 'fecha_hora': time.strftime("%c")})
            return {'token': str(result.inserted_id),'grupo':usuario_json['grupo_id'],'usuario': usuario_json['usuario']}

    def delete(self,token):
        db.token.remove({'_id': ObjectId(token)})
        return {'Sesion':'conexion finalizada'}