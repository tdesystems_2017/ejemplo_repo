from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

client = MongoClient()
db = client.sistema

app = Flask(__name__)

class Recurso_base():

	def __init__(self,token):
		result = db.token.find({"_id":ObjectId(token)})
		numero_token = result.count()
		print('hola')
		if numero_token == 0:
			return {"Error":"Sin sesion iniciada"}, 401
		