from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId
#from recurso_base import Recurso_base
import json


client = MongoClient()
db = client.sistema

app = Flask(__name__)


class Grupo(Resource):

    def post(self,token):
        try:
            resultado = db.token.find({'_id':ObjectId(token)})
            numero_token = resultado.count()
        except Exception as e:
            return {'mensaje': 'Acceso denegado'}, 401
        if numero_token == 0:
            return {'mensaje':'Acceso denegado'}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('observaciones', required=True)
        parser.add_argument('status', required=True)
        args = parser.parse_args()
        descripcion = args['descripcion']
        observaciones = args['observaciones']
        status = args['status']
        db.grupo.insert({'descripcion': descripcion, 'observaciones': observaciones, 'status': status})
        return {'mensaje': 'post', 'descripcion': descripcion, 'observaciones': observaciones, 'status': status,'numero': numero_token}

    def get(self,token):
        try:
            resultado = db.token.find({'_id':ObjectId(token)})
            numero_token = resultado.count()
        except Exception as e:
            return {'mensaje': 'Acceso denegado'}, 401
        if numero_token == 0:
            return {'mensaje':'Acceso denegado'}, 401


        for grupo_valida in resultado:
            validacion_grupo = grupo_valida['grupo']

        #print validacion_grupo
        resultado = db.permiso.find({'grupo':validacion_grupo,'seccion':'grupo','accion':'get'})
        n_permisos = resultado.count()
        print n_permisos

        if n_permisos == 0:
            return {'mensaje': 'Acceso denegado'}, 401


        grupos = db.grupo.find()
        grupos_enviar = []
        for grupo in grupos:
            grupo_json = {}
            grupo_json['descripcion'] = grupo['descripcion']
            grupo_json['observaciones'] = grupo['observaciones']
            grupo_json['status'] = grupo['status']
            grupo_json['_id'] = str(grupo['_id'])
            grupos_enviar.append(grupo_json)
        return {'grupo': grupos_enviar}

    def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('observaciones', required=True)
        parser.add_argument('status', required=True)
        args = parser.parse_args()
        descripcion_update = args['descripcion']
        observaciones_update = args['observaciones']
        status_update = args['status']
        db.grupo.update({'_id': ObjectId(id)}, {'$set': {'descripcion': descripcion_update, 'observaciones': observaciones_update, 'status': status_update}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, id):
        db.grupo.remove({'_id': ObjectId(id)})
        return {'mensaje': 'delete'}
