from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json

client = MongoClient()
db = client.sistema

app = Flask(__name__)

class Permiso(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('seccion', required=True)
        parser.add_argument('accion', required=True)
        parser.add_argument('grupo', required=True)
        parser.add_argument('grupo_id', required=True)
        args = parser.parse_args()
        seccion = args['seccion']
        accion = args['accion']
        grupo = args['grupo']
        grupo_id = args['grupo_id']
        db.permiso.insert({'seccion': seccion, 'accion': accion, 'grupo': grupo,'grupo_id':grupo_id})
        return {'seccion': seccion, 'accion': accion, 'grupo': grupo,'grupo_id':grupo_id}

    def get(self):
        permisos = db.permiso.find()
        permisos_enviar = []
        for permiso in permisos:
            permiso_json = {}
            permiso_json['seccion'] = permiso['seccion']
            permiso_json['accion'] = permiso['accion']
            permiso_json['grupo'] = permiso['grupo']
            permiso_json['grupo_id'] = permiso['grupo_id']
            permiso_json['_id'] = str(permiso['_id'])
            permisos_enviar.append(permiso_json)
        return {'token': permisos_enviar}

    def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument('seccion', required=True)
        parser.add_argument('accion', required=True)
        parser.add_argument('grupo', required=True)
        parser.add_argument('grupo_id', required=True)
        args = parser.parse_args()
        seccion = args['seccion']
        accion = args['accion']
        grupo = args['grupo']
        grupo_id = args['grupo_id']
        db.permiso.update({'_id': ObjectId(id)}, {'$set': {'seccion': seccion, 'accion': accion, 'grupo': grupo, 'grupo_id': grupo_id}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, id):
        db.permiso.remove({'_id': ObjectId(id)})
        return {'mensaje': 'delete'}
