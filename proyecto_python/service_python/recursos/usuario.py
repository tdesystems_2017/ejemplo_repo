from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json

client = MongoClient()
db = client.sistema

app = Flask(__name__)

class Usuario(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('usuario', required=True)
        parser.add_argument('contrasena', required=True)
        parser.add_argument('personal_id', required=True)
        parser.add_argument('grupo_id', required=True)
        args = parser.parse_args()
        usuario = args['usuario']
        contrasena = args['contrasena']
        personal_id = args['personal_id']
        grupo_id = args['grupo_id']
        db.usuario.insert({'usuario': usuario, 'contrasena': contrasena, 'personal_id': personal_id, 'grupo_id': grupo_id})
        return {'mensaje': 'post', 'usuario': usuario, 'contrasena': contrasena, 'personal_id': personal_id, 'grupo_id': grupo_id}

    def get(self):
        usuarios = db.usuario.find()
        usuarios_enviar = []
        for usuario in usuarios:
            usuario_json = {}
            usuario_json['usuario'] = usuario['usuario']
            usuario_json['contrasena'] = usuario['contrasena']
            usuario_json['personal_id'] = usuario['personal_id']
            usuario_json['grupo_id'] = usuario['grupo_id']
            usuario_json['_id'] = str(usuario['_id'])
            usuarios_enviar.append(usuario_json)
        return {'usuario': usuarios_enviar}

    def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument('usuario', required=True)
        parser.add_argument('contrasena', required=True)
        parser.add_argument('personal_id', required=True)
        parser.add_argument('grupo_id', required=True)
        args = parser.parse_args()
        usuario_update = args['usuario']
        contrasena_update = args['contrasena']
        personal_id_update = args['personal_id']
        grupo_id = args['grupo_id']
        db.usuario.update({'_id': ObjectId(id)}, {'$set': {'usuario': usuario_update, 'contrasena': contrasena_update, 'personal_id': personal_id_update, 'grupo_id': grupo_id}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, id):
        db.usuario.remove({'_id': ObjectId(id)})
        return {'mensaje': 'delete'}
