from flask import Flask
from flask_restful import Resource,reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json

client = MongoClient()
db = client.sistema

app = Flask(__name__)

class Paises(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('pais',required=True)
        args = parser.parse_args()
        pais = args['pais']
        db.pais.insert({'descripcion':pais})
        return {'mensaje':'post','pais':pais}
    def get(self):
        paises = db.pais.find()
        paises_enviar = []
        for pais in paises:
            pais_json = {}
            pais_json['id'] = str(pais['_id'])
            pais_json['descripcion'] = pais['descripcion']
            paises_enviar.append(pais_json)
        return {'mensaje':'get','paises':paises_enviar}
    def put(self,pais):
        parser = reqparse.RequestParser()
        parser.add_argument('pais', required=True)
        args = parser.parse_args()
        pais_update = args['pais']
        db.pais.update({'_id':ObjectId(pais)},{'$set':{'descripcion':pais_update}})
        return {'mensaje':'put'}
    def patch(self):
        return {'mensaje':'patch'}
    def delete(self,pais):
        db.pais.remove({'descripcion':pais})
        return {'mensaje':'delete','pais':pais}