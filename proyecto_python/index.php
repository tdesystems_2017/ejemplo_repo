<!DOCTYPE html>
<html ng-app="app">
<head>
	<title>Sistema Agro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular.min.js"></script>
    <script src='//ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular-route.min.js'></script>
    <script src='//ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular-cookies.js'></script>
    <script src="//code.angularjs.org/1.2.19/i18n/angular-locale_es-es.js"></script>
	<link rel="stylesheet" type="text/css" href="/static/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/static/css/style.css">
	<script src="/static/js/jQuery/jquery.min.js"></script>
	<script src="/static/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container wrapper">
		<h1 class="text-center">Sistema Agro</h1>
		<div class="row">
	    	<div class="col-md-12">
	            <div class="panel with-nav-tabs panel-default">
	                <div class="panel-heading">
	                        <ul class="nav nav-tabs">
	                            <li><a href="#/home">Home</a></li>
	                            <li><a href="#/seccion/grupo">Grupo</a></li>
	                            <li><a href="" ng-click="logout()">Salir</a></li>
	                        </ul>
	                </div>
	                <div class="panel-body">
	                    <div class="tab-content">
	                        <div class="tab-pane fade in active" id="home">
	                        </div>
	                        <div ng-view></div>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
	<script src="/static/js/angular-script.js"></script>
</body>
</html>


