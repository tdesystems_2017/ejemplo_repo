<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<form class="form-horizontal alert alert-warning" name="loginList" id="loginForm" ng-submit="login(loginInfo);">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-5 col-md-3">
                <div class="form-login_sesion">
                <h4>Bienvenido</h4>
                <input type="text" name="usuario" class="form-control input-sm chat-input" placeholder="Usuario" ng-model="loginInfo.usuario" />
                </br>
                <input type="password" name="contrasena" class="form-control input-sm chat-input" placeholder="Contraseña" ng-model="loginInfo.contrasena"/>
                </br>
                <div class="wrapper_sesion">
                <span class="group-btn">
                    <button class="btn btn-primary btn-md">Inicio <i class="fa fa-sign-in"></i></button>
                </span>
                </div>
                </div>
            </div>
        </div>
    </div>
</form>