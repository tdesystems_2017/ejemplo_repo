	<div ng-controller="grupoController">
		<nav class="navbar navbar-default">
			<div class="navbar-header">
				<div class="alert alert-default navbar-brand search-box">
					<button class="btn btn-primary" ng-show="show_form" ng-click="formToggle()">Agregar Grupo
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
					</button>
				</div>
				<div class="alert alert-default input-group search-box">
					<span class="input-group-btn">
					<input type="text" class="form-control" placeholder="Busqueda Grupo ..." ng-model="search_query">
					</span>
				</div>
			</div>
		</nav>
		<div class="col-md-6 col-md-offset-3">
			<div ng-include src="'/proyecto_python/templates/views/grupo/alta.php'"></div>
			<div ng-include src="'/proyecto_python/templates/views/grupo/modifica.php'"></div>
		</div>
		<div class="clearfix"></div>
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<th>Id</th>
					<th>Descripcion</th>
					<th>Observaciones</th>
					<th>Status</th>
					<th></th>
					<th></th>
				</thead>
				<tbody>
					<tr ng-repeat="grupo in grupos| filter:search_query">
						<td><span>{{grupo._id}}</span></td>
						<td>{{grupo.descripcion}}</td>
						<td>{{grupo.observaciones}}</td>
						<td>{{grupo.status}}</td>
						<td>
						<button class="btn btn-warning" ng-click="editarInfo(grupo)" title="Modificar"><span class="glyphicon glyphicon-edit"></span></button>
						</td>
						<td>
						<button class="btn btn-danger" ng-click="elimina(grupo)" title="Eliminar"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>