<form class="form-horizontal alert alert-warning" id="modificaForm" ng-submit="modificaInfo(modifica)" hidden>
<h3 class="text-center">Modifica / Grupo</h3>
	<div class="form-group">
		<label for="Descripcion">Descripcion:</label>
		<input type="text" class="form-control" ng-model="modifica.descripcion" value="{{modifica.descripcion}}">
	</div>
	<div class="form-group">
		<label for="Observaciones">Observaciones:</label>
		<input type="text" class="form-control" ng-model="modifica.observaciones" value="{{modifica.observaciones}}">
	</div>
	<div class="form-group text-center">
		<label for="Status">Status:</label>
		<label for="" class="radio-inline status">
			<input type="radio" ng-model="modifica.status" value="0" >Inactivo
		</label>
		<label for="" class="radio-inline status">
			<input type="radio" name="status" ng-model="modifica.status" value="1">Activo
		</label>
	</div>
	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="grupoList.$invalid" ng-click="modificaMsg(modifica._id)">Enviar</button>
	</div>
</form>
