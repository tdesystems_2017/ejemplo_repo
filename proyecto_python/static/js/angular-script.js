var app = angular.module('app',['ngRoute','ngCookies']);

app.config(['$routeProvider',function($routeProvider) {
  
	$routeProvider.when('/', {
		templateUrl: "/proyecto_python/templates/views/sesion/login.php",
		controller: "sesionController"
	});

	$routeProvider.when('/login', {
		controller: "sesionController"
	});

	$routeProvider.when('/home', {
		templateUrl: "/proyecto_python/templates/views/sesion/inicio.php",
		controller: "bienvenidoController"
	});

	$routeProvider.when('/seccion/grupo', {
		templateUrl: "/proyecto_python/templates/views/grupo/lista.php",
		controller: "grupoController"
	});

}]);

app.controller("bienvenidoController", ["$scope","$routeParams",'$http',"auth","$cookies",function($scope,$routeParams,$http,$auth,$cookies) {
	//alert($cookies.token);
	$scope.datos_usuario = document.cookie;
	$auth.checkStatus();
	$scope.logout = function()
    {
        $auth.logout();
    }

}]);

app.controller("grupoController", ["$scope","$routeParams",'$http',"auth","$cookies",function($scope,$routeParams,$http,$auth,$cookies) {

	$auth.checkStatus();

	$scope.logout = function()
    {
        $auth.logout();
    }

   	$scope.show_form = true;
	$scope.formToggle =function(){
		$scope.grupoInfo = {'status' : '1','descripcion' : '','observaciones' : ''};
		$('#altaForm').slideToggle();
		$('#modificaForm').css('display', 'none');
	}

	$scope.alta = function(info){
			var datos;
			
			datos = {"descripcion":info.descripcion,"observaciones":info.observaciones,"status":info.status};

			$http({
		        method: 'POST',
		        data: datos,
		        url: '/service_python/grupo',
		    }).then(function(response) {
		        $('#altaForm').css('display', 'none');
		        listaGrupo();
		    }, function(error) {
		        console.log(error);
		    });
	}

   	listaGrupo();
   	function listaGrupo() {
	    $http({
	        method: 'GET',
	        url: '/service_python/grupo/token/'+$cookies.token,
	    }).then(function(response) {
	        $scope.grupos = response.data.grupo;
	        //console.log('mm', $scope.grupos);
	    }, function(error) {
	        console.log(error);
	    });
	}

	$scope.modifica = {};
	$scope.editarInfo = function(info){
		$scope.modifica = info;
		$('#altaForm').slideUp();
		$('#modificaForm').slideToggle();
	}
	$scope.modificaInfo = function(info){
			var datos;
			datos = {"descripcion":info.descripcion,"observaciones":info.observaciones,"status":info.status};
			//console.log(info._id);
			$http({
		        method: 'PUT',
		        data: datos,
		        url: '/service_python/grupo/'+info._id,
		    }).then(function(response) {
		        $('#altaForm').css('display', 'none');
		        listaGrupo();
		    }, function(error) {
		        console.log(error);
		    });
	}

	$scope.modificaMsg = function(id){
			$('#modificaForm').css('display', 'none');
	}

	$scope.elimina = function(info){
			$http({
		        method: 'DELETE',
		        url: '/service_python/grupo/'+info._id,
		    }).then(function(response) {
		    	listaGrupo();
		        //console.log();
		    }, function(error) {
		        console.log(error);
		    });
	}

}]);

app.factory("auth", function($cookies,$cookieStore,$location,$http){
    return{
        login : function(usuario, token,grupo)
        {
            $cookies.usuario = usuario;
            $cookies.token = token;
            $cookies.grupo = grupo;
            $location.path("/home");
        },
        logout : function()
        {
            $http({
		        method: 'DELETE',
		        url: '/service_python/sesion/'+$cookies.token,
		    }).then(function(response) {
		        $cookieStore.remove("usuario");
	            $cookieStore.remove("token");
	            $cookieStore.remove("grupo");
	            $location.path("/login");
		    }, function(error) {
		        console.log(error);
		    });
        },
        checkStatus : function()
        {
        	//console.log($cookies.usuario);

            var rutasPrivadas = ["/home","/sesion","/seccion/grupo","/login"];
            if(this.in_array($location.path(),rutasPrivadas) && typeof($cookies.usuario) == "undefined")
            {
                $location.path("/");
            }

            if(this.in_array("/",rutasPrivadas) && typeof($cookies.usuario) != "undefined")
            {
                $location.path("/home");
            }
        },
        in_array : function(needle, haystack)
        {
            var key = '';
            for(key in haystack)
            {
                if(haystack[key] == needle)
                {
                    return true;
                }
            }
            return false;
        }
    }
});

app.controller("sesionController", ["$scope","$routeParams",'$http','auth',function($scope,$routeParams,$http,$auth) {

	$auth.checkStatus();

	$scope.login = function(info){
			var datos;
			
			datos = {"usuario":info.usuario,"contrasena":info.contrasena};
			//console.log(info);
			$http({
		        method: 'GET',
		        url: '/service_python/sesion/'+info.usuario+'/'+info.contrasena,
		    }).then(function(response) {
		    	$auth.login(response.data.usuario, response.data.token,response.data.grupo);
		    }, function(error) {
		        console.log(error);
		    });
	}

}]);

