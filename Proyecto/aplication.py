from flask import Flask
from flask_restful import Api
app = Flask(__name__)


from recursos.paises import Paises
from recursos.estados import Estados
from recursos.grupo import Grupo

api = Api(app)

api.add_resource(Paises, '/paises', '/paises/<string:pais>')
api.add_resource(Estados, '/estados', '/estados/<string:estado>')
api.add_resource(Grupo, '/grupo', '/grupo/<string:grupo>')
if __name__ == '__main__':
    app.run(host='0.0.0.0',debug= True, port=5000)