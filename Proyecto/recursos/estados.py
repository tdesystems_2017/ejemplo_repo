from flask_restful import Resource, reqparse


class Estados(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('estado', required=True)
        args=parser.parse_args()
        estado = args['estado']

        return {'mensaje': 'post', 'estado': estado}

    def get(self,estado):
        return {'mensaje': 'get', 'estado': estado}

    def put(self):
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self):
        return {'mensaje':'delete'}
