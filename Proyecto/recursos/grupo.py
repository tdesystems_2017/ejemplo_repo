from flask import Flask
from flask_restful import Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json

client = MongoClient()
db = client.sistema

app = Flask(__name__)



class Grupo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('observaciones', required=True)
        parser.add_argument('status', required=True)
        args = parser.parse_args()

        descripcion = args['descripcion']
        observaciones = args['observaciones']
        status = args['status']

        db.grupo.insert({'descripcion':descripcion,'observaciones':observaciones,'status': status})

        return {'mensaje': 'post', 'Consulta': 'registrado'}

    def get(self):

        grupos = db.grupo.find()

        grupo_enviar = []
        for grupo in grupos:
            grupo_json = {}
            grupo_json['id'] = str(grupo['_id'])
            grupo_json['descripcion'] = grupo['descripcion']
            grupo_json['observaciones'] = grupo['observaciones']
            grupo_json['status'] = grupo['status']
            grupo_enviar.append(grupo_json)

        return {'mensaje': 'get', 'Grupo': grupo_enviar}

    def put(self, grupo):
        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('observaciones', required=True)
        parser.add_argument('status', required=True)
        args = parser.parse_args()

        descripcion = args['descripcion']
        observaciones = args['observaciones']
        status = args['status']

        db.grupo.update({'_id': ObjectId(grupo)}, {'$set': {'descripcion': descripcion,'observaciones': observaciones, 'status': status}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}



    def delete(self,grupo):
        db.grupo.remove({'_id':ObjectId(grupo)})
        return {'mensaje':'delete','grupo':grupo}





