<?php
require_once('./config/conexion.php');

class modelo_grupo{

	public function alta_db($registro=False, $tabla=False){
		if(is_array($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos();
			$link = $conexion->link;
		}
		else{
			return false;
		}
		$valores = '';
		for($i=0;$i<count($registro);$i++){
			$valores .= "'".$registro[$i]."'";
			if( ($i+1) !== count($registro) ){
				$valores .= ",";
			}
		}
		$consulta_insercion = "INSERT INTO ". $tabla." (descripcion,observaciones,status) VALUES (".$valores.")";
		$link->query($consulta_insercion);
		if($link->error){
			return false;
		}
		else{
			return true;
		}
	}

	public function elimina_db($registro=False, $tabla=False){
		if(!empty($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos();
			$link = $conexion->link;
		}
		else{
			return false;
		}
		$consulta_elimina = "DELETE FROM ". $tabla." WHERE id='".$registro."'";
		$link->query($consulta_elimina);
		if($link->error){
			return false;
		}
		else{
			return true;
		}
	}

	public function modifica_db($registro=False,$id=False,$tabla=False){
		if(is_array($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos();
			$link = $conexion->link;
		}
		else{
			return false;
		}
		$valores = '';
		$campos = array('descripcion','observaciones','status');
		for($i=0;$i<count($registro);$i++){
			$valores .= $campos[$i]."='".$registro[$i]."'";
			if( ($i+1) !== count($registro) ){
				$valores .= ",";
			}
		}
		$consulta_modifica = "UPDATE ". $tabla." SET ".$valores." WHERE id=".$id;
		$link->query($consulta_modifica);
		if($link->error){
			return false;
		}
		else{
			return true;
		}
	}

	public function obten_por_id($id=False,$tabla=False){
		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$link = $conexion->link;

		$consulta = "SELECT * FROM ".$tabla." WHERE id = ".$id;
		$result = $link->query($consulta);

		if($link->error){
			return false;
		}

		$tabla_base = array('id' => 'id','descripcion' => 'descripcion','observaciones' => 'observaciones','status' => 'status');
		$resultado = array();

		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado[$campo] = $row[$campo];
			}
			$i++;
		}
		
		if(!empty($resultado)){
			return $resultado;
		}else{
		    return false;
		}
	}

	public function lista($tabla=False){
		if(!empty($tabla)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos();
			$link = $conexion->link;
		}
		else{
			return false;
		}

		$consulta = "SELECT * FROM ".$tabla;
		$result = $link->query($consulta);

		if($link->error){
			return false;
		}
		$tabla_base = array('id' => 'id','descripcion' => 'descripcion','observaciones' => 'observaciones','status' => 'status');
		$i=0;
		$resultado = array();
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado[$i][$campo] = $row[$campo];
			}
			$i++;
		}

		return $resultado;
	}

}

?>