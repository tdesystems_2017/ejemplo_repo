<?php
require_once('./lib/nusoap.php');
require_once('./controladores/controlador_seccion.php');
      
    $server = new nusoap_server();
    $ns = 'http://localhost/angular_crud/servidor/nusoap';
    $server->configureWSDL('Server Seccion',$ns);
    $server->wsdl->schemaTargetNamespace=$ns;
      
    $server->register("alta_db",
        array("registro" => "xsd:Array","tabla" => "xsd:string"),
        array("return" => "xsd:string"));

    $server->register("elimina_db",
        array("registro" => "xsd:string","tabla" => "xsd:string"),
        array("return" => "xsd:string"));

    $server->register("modifica_db",
        array("registro" => "xsd:Array","id" => "xsd:string","tabla" => "xsd:string"),
        array("return" => "xsd:string"));

    $server->register("obten_por_id",
        array("id" => "xsd:string","tabla" => "xsd:string"),
        array("return" => "xsd:Array"));

    $server->register("lista",
        array("tabla" => "xsd:string"),
        array("return" => "xsd:Array"));

    @$server->service(file_get_contents("php://input"));
?>