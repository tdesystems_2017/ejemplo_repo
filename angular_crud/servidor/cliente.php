<?php
	require_once('./lib/nusoap.php');

	if(!isset($_GET['seccion'])){
		$seccion='';	
	}
	else{
		$seccion = $_GET['seccion'];
		$cliente = new nusoap_client('http://localhost/angular_crud/servidor/server_'.$seccion.'.php?wsdl','wsdl');
	}
	if(!isset($_GET['accion'])){
		$accion='';	
	}
	else{
		$accion = $_GET['accion'];
	}

	if(empty($accion) || empty($seccion)){
		header("Location: http://localhost/angular_crud/");
	}

	if($accion == 'lista'){
		$resultado=$cliente->call($accion,array('tabla' => $seccion));
		echo json_encode($resultado);
	}

	if($accion == 'alta_db'){
		$data = json_decode(file_get_contents("php://input"));
		if($seccion == 'grupo' || $seccion == 'seccion'){
			$descripcion = $data->descripcion;
			$observaciones = $data->observaciones;
			$status = $data->status;
			$array = array($descripcion,$observaciones,$status);
		}else if($seccion == 'accion'){
			$descripcion = $data->descripcion;
			$seccion_id = $data->seccion_id;
			$array = array($descripcion,$seccion_id);
		}else if($seccion == 'accion_grupo'){
			$accion_id = $data->accion_id;
			$grupo_id = $data->grupo_id;
			$array = array($accion_id,$grupo_id);
		}

		$resultado=$cliente->call($accion,array('registro'=> $array,'tabla' => $seccion));
		if($resultado){
			echo true;
		}
	}

	if($accion == 'elimina_db'){
		$data = json_decode(file_get_contents("php://input"));
		$id = $data->id;

		$resultado=$cliente->call($accion,array('registro'=> $id, 'tabla' => $seccion));

		if($resultado){
			echo true;
		}
	}

	if($accion == 'modifica_db'){
		$data = json_decode(file_get_contents("php://input"));
		$id = $data->id;
		if($seccion == 'grupo' || $seccion == 'seccion'){
			$descripcion = $data->descripcion;
			$observaciones = $data->observaciones;
			$status = $data->status;
			$array = array($descripcion,$observaciones,$status);
		}else if($seccion == 'accion'){
			$descripcion = $data->descripcion;
			$seccion_id = $data->seccion_id;
			$array = array($descripcion,$seccion_id);
		}

		$resultado=$cliente->call($accion,array('registro'=> $array,'id' => $id, 'tabla' => $seccion));
		if($resultado){
			echo true;
		}
	}

	if($accion == 'obten_por_id'){
		$resultado=$cliente->call('obten_por_id',array('id'=> 44, 'tabla' => $seccion));

		$json = json_encode($resultado);
		print_r($json);
	}
	
?>
