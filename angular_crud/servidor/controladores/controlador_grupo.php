<?php
	require_once './modelos/grupo.php';

	function alta_db($registro=false,$tabla=false){		
			$modelo_grupo = new modelo_grupo();
			$resultado = $modelo_grupo->alta_db($registro,$tabla);
			return $resultado;
	}
	
	function elimina_db($registro=false,$tabla=false){		
			$modelo_grupo = new modelo_grupo();
			$resultado = $modelo_grupo->elimina_db($registro,$tabla);
			return $resultado;
	}

	function modifica_db($registro=false,$id=false,$tabla=false){		
			$modelo_grupo = new modelo_grupo();
			$resultado = $modelo_grupo->modifica_db($registro,$id,$tabla);
			return $resultado;
	}

	function obten_por_id($id=False,$tabla=False){
			$modelo_grupo = new modelo_grupo();
			$resultado = $modelo_grupo->obten_por_id($id,$tabla);
			return $resultado;
	}

	function lista($tabla=False){
			$modelo_grupo = new modelo_grupo();
			$resultado = $modelo_grupo->lista($tabla);
			return $resultado;
	}

?>
