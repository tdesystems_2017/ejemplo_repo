<?php
	require_once './modelos/accion_grupo.php';

	function alta_db($registro=false,$tabla=false){		
			$modelo_accion_grupo = new modelo_accion_grupo();
			$resultado = $modelo_accion_grupo->alta_db($registro,$tabla);
			return $resultado;
	}
	
	function elimina_db($registro=false,$tabla=false){		
			$modelo_accion_grupo = new modelo_accion_grupo();
			$resultado = $modelo_accion_grupo->elimina_db($registro,$tabla);
			return $resultado;
	}

	function modifica_db($registro=false,$id=false,$tabla=false){		
			$modelo_accion_grupo = new modelo_accion_grupo();
			$resultado = $modelo_accion_grupo->modifica_db($registro,$id,$tabla);
			return $resultado;
	}

	function obten_por_id($id=False,$tabla=False){
			$modelo_accion_grupo = new modelo_accion_grupo();
			$resultado = $modelo_accion_grupo->obten_por_id($id,$tabla);
			return $resultado;
	}

	function lista($tabla=False){
			$modelo_accion_grupo = new modelo_accion_grupo();
			$resultado = $modelo_accion_grupo->lista($tabla);
			return $resultado;
	}

?>