<?php
	require_once './modelos/seccion.php';

	function alta_db($registro=false,$tabla=false){		
			$modelo_seccion = new modelo_seccion();
			$resultado = $modelo_seccion->alta_db($registro,$tabla);
			return $resultado;
	}
	
	function elimina_db($registro=false,$tabla=false){		
			$modelo_seccion = new modelo_seccion();
			$resultado = $modelo_seccion->elimina_db($registro,$tabla);
			return $resultado;
	}

	function modifica_db($registro=false,$id=false,$tabla=false){		
			$modelo_seccion = new modelo_seccion();
			$resultado = $modelo_seccion->modifica_db($registro,$id,$tabla);
			return $resultado;
	}

	function obten_por_id($id=False,$tabla=False){
			$modelo_seccion = new modelo_seccion();
			$resultado = $modelo_seccion->obten_por_id($id,$tabla);
			return $resultado;
	}

	function lista($tabla=False){
			$modelo_seccion = new modelo_seccion();
			$resultado = $modelo_seccion->lista($tabla);
			return $resultado;
	}

?>