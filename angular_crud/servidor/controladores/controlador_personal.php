<?php
	require_once './modelos/personal.php';

	function alta_db($registro=false,$tabla=false){		
			$modelo_personal = new modelo_personal();
			$resultado = $modelo_personal->alta_db($registro,$tabla);
			return $resultado;
	}
	
	function elimina_db($registro=false,$tabla=false){		
			$modelo_personal = new modelo_personal();
			$resultado = $modelo_personal->elimina_db($registro,$tabla);
			return $resultado;
	}

	function modifica_db($registro=false,$id=false,$tabla=false){		
			$modelo_personal = new modelo_personal();
			$resultado = $modelo_personal->modifica_db($registro,$id,$tabla);
			return $resultado;
	}

	function obten_por_id($id=False,$tabla=False){
			$modelo_personal = new modelo_personal();
			$resultado = $modelo_personal->obten_por_id($id,$tabla);
			return $resultado;
	}

	function lista($tabla=False){
			$modelo_personal = new modelo_personal();
			$resultado = $modelo_personal->lista($tabla);
			return $resultado;
	}

?>