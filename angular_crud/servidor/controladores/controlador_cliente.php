<?php
	require_once './modelos/cliente.php';

	function alta_db($registro=false,$tabla=false){		
			$modelo_cliente = new modelo_cliente();
			$resultado = $modelo_cliente->alta_db($registro,$tabla);
			return $resultado;
	}
	
	function elimina_db($registro=false,$tabla=false){		
			$modelo_cliente = new modelo_cliente();
			$resultado = $modelo_cliente->elimina_db($registro,$tabla);
			return $resultado;
	}

	function modifica_db($registro=false,$id=false,$tabla=false){		
			$modelo_cliente = new modelo_cliente();
			$resultado = $modelo_cliente->modifica_db($registro,$id,$tabla);
			return $resultado;
	}

	function obten_por_id($id=False,$tabla=False){
			$modelo_cliente = new modelo_cliente();
			$resultado = $modelo_cliente->obten_por_id($id,$tabla);
			return $resultado;
	}

	function lista($tabla=False){
			$modelo_cliente = new modelo_cliente();
			$resultado = $modelo_cliente->lista($tabla);
			return $resultado;
	}

?>