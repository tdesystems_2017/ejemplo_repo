<?php
	require_once './modelos/accion.php';

	function alta_db($registro=false,$tabla=false){		
			$modelo_accion = new modelo_accion();
			$resultado = $modelo_accion->alta_db($registro,$tabla);
			return $resultado;
	}
	
	function elimina_db($registro=false,$tabla=false){		
			$modelo_accion = new modelo_accion();
			$resultado = $modelo_accion->elimina_db($registro,$tabla);
			return $resultado;
	}

	function modifica_db($registro=false,$id=false,$tabla=false){		
			$modelo_accion = new modelo_accion();
			$resultado = $modelo_accion->modifica_db($registro,$id,$tabla);
			return $resultado;
	}

	function obten_por_id($id=False,$tabla=False){
			$modelo_accion = new modelo_accion();
			$resultado = $modelo_accion->obten_por_id($id,$tabla);
			return $resultado;
	}

	function lista($tabla=False){
			$modelo_accion = new modelo_accion();
			$resultado = $modelo_accion->lista($tabla);
			return $resultado;
	}

?>