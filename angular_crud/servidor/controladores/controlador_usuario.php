<?php
	require_once './modelos/usuario.php';

	function alta_db($registro=false,$tabla=false){		
			$modelo_usuario = new modelo_usuario();
			$resultado = $modelo_usuario->alta_db($registro,$tabla);
			return $resultado;
	}
	
	function elimina_db($registro=false,$tabla=false){		
			$modelo_usuario = new modelo_usuario();
			$resultado = $modelo_usuario->elimina_db($registro,$tabla);
			return $resultado;
	}

	function modifica_db($registro=false,$id=false,$tabla=false){		
			$modelo_usuario = new modelo_usuario();
			$resultado = $modelo_usuario->modifica_db($registro,$id,$tabla);
			return $resultado;
	}

	function obten_por_id($id=False,$tabla=False){
			$modelo_usuario = new modelo_usuario();
			$resultado = $modelo_usuario->obten_por_id($id,$tabla);
			return $resultado;
	}

	function lista($tabla=False){
			$modelo_usuario = new modelo_usuario();
			$resultado = $modelo_usuario->lista($tabla);
			return $resultado;
	}

?>