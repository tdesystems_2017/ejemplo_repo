<?php
	require_once './modelos/proyecto.php';

	function alta_db($registro=false,$tabla=false){		
			$modelo_proyecto = new modelo_proyecto();
			$resultado = $modelo_proyecto->alta_db($registro,$tabla);
			return $resultado;
	}
	
	function elimina_db($registro=false,$tabla=false){		
			$modelo_proyecto = new modelo_proyecto();
			$resultado = $modelo_proyecto->elimina_db($registro,$tabla);
			return $resultado;
	}

	function modifica_db($registro=false,$id=false,$tabla=false){		
			$modelo_proyecto = new modelo_proyecto();
			$resultado = $modelo_proyecto->modifica_db($registro,$id,$tabla);
			return $resultado;
	}

	function obten_por_id($id=False,$tabla=False){
			$modelo_proyecto = new modelo_proyecto();
			$resultado = $modelo_proyecto->obten_por_id($id,$tabla);
			return $resultado;
	}

	function lista($tabla=False){
			$modelo_proyecto = new modelo_proyecto();
			$resultado = $modelo_proyecto->lista($tabla);
			return $resultado;
	}

?>