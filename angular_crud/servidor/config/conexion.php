<?php
class Conexion{
	public $nombre_base_datos = 'sistema_agro';
	private $host = 'localhost';
	private $user = 'root';
	private $pass = '1qa2ws3ed'; 
	public $link;
	function __construct(){
		$this->link = mysqli_connect($this->host, $this->user, $this->pass);
	}
	public function selecciona_base_datos($nombre_base_datos=false){
		if($nombre_base_datos){
			$this->nombre_base_datos = $nombre_base_datos;
		}
		$consulta = "USE ".$this->nombre_base_datos;
		$this->link->query($consulta);
		if($this->link->error){
			print_r($this->link->error);
		}
		else{
			//print_r("Se selecionó la base de datos: ".$nombre_base_datos);
		}
	}
}

$m = new Conexion();
$m->selecciona_base_datos();

?>