var crudApp = angular.module('crudApp',[]);
crudApp.controller("DbController",['$scope','$http', function($scope,$http){

var url = new URL(window.location.href);
$scope.seccion = url.searchParams.get("seccion");
$scope.accion = url.searchParams.get("accion");
//console.log($scope.accion+$scope.seccion);

$scope.grupoInfo = {'status' : '1'};
$scope.seccionInfo = {'status' : '1'};
$scope.show_form = true;
$scope.formToggle =function(){
	$scope.grupoInfo = {'status' : '1','descripcion' : '','observaciones' : ''};
	$scope.seccionInfo = {'status' : '1','descripcion' : '','observaciones' : ''};
	$scope.accionInfo = {'descripcion' : '','seccion_id':''};
	$scope.accion_grupoInfo = {'accion_id' : '','grupo_id':''};
	$('#altaForm').slideToggle();
	$('#modificaForm').css('display', 'none');
}

listaInfo($scope.seccion,$scope.accion);
function listaInfo(seccion,accion){
	if(seccion != '' && accion != ''){
		$http.post('servidor/cliente.php?seccion='+seccion+'&accion='+accion+'').success(function(data){
			$scope.registros = data;
		});
	}
	if(seccion == 'accion'){
		$http.post('servidor/cliente.php?seccion=seccion&accion='+accion+'').success(function(data){
			$scope.registros_seccion = data;
		});
	}
	if(seccion == 'accion_grupo'){
		$http.post('servidor/cliente.php?seccion=grupo&accion='+accion+'').success(function(data){
			$scope.registros_grupo = data;
		});
		$http.post('servidor/cliente.php?seccion=accion&accion='+accion+'').success(function(data){
			$scope.registros_accion = data;
		});
	}
}

$scope.alta = function(info,seccion,accion){
		var datos;
		if(seccion == 'grupo' || seccion == 'seccion'){
			datos = {"descripcion":info.descripcion,"observaciones":info.observaciones,"status":info.status};
		}else if(seccion == 'accion'){
			datos = {"descripcion":info.descripcion,"seccion_id":info.seccion_id};
		}else if(seccion == 'accion_grupo'){
			datos = {"accion_id":info.accion_id,"grupo_id":info.grupo_id};
		}
		console.log(datos);
		//console.log({"descripcion":info.descripcion,"observaciones":info.observaciones,"status":info.status});
		$http.post('servidor/cliente.php?seccion='+seccion+'&accion='+accion+'',datos).success(function(data){
		console.log('servidor/cliente.php?seccion='+seccion+'&accion='+accion+'');
		if (data == true) {
			listaInfo($scope.seccion,$scope.accion);
			$('#altaForm').css('display', 'none');
		}
	});
}
$scope.elimina = function(info,seccion,accion){
		$http.post('servidor/cliente.php?seccion='+seccion+'&accion='+accion+'',{"id":info.id}).success(function(data){
		if (data == true) {
			listaInfo($scope.seccion,$scope.accion);
		}
	});
}
$scope.modifica = {};
$scope.editarInfo = function(info){
	$scope.modifica = info;
	$('#altaForm').slideUp();
	$('#modificaForm').slideToggle();
}
$scope.modificaInfo = function(info,seccion,accion){
		var datos;
		if(seccion == 'grupo' || seccion == 'seccion'){
			datos = {"id":info.id,"descripcion":info.descripcion,"observaciones":info.observaciones,"status":info.status};
		}else if(seccion == 'accion'){
			datos = {"id":info.id,"descripcion":info.descripcion,"seccion_id":info.seccion_id};
		}
		console.log(datos);
		//{"id":info.id,"descripcion":info.descripcion,"observaciones":info.observaciones,"status":info.status}
		$http.post('servidor/cliente.php?seccion='+seccion+'&accion='+accion+'',datos).success(function(data){
		$scope.show_form = true;
		if (data == 1) {
			listaInfo($scope.seccion,$scope.accion);
		}
	});
}

$scope.modificaMsg = function(id){
		$('#modificaForm').css('display', 'none');
}

}]);