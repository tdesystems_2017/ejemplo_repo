CREATE DATABASE IF NOT EXISTS sistema_agro;

USE sistema_agro;

DROP TABLE IF EXISTS accion_grupo;
DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS accion;
DROP TABLE IF EXISTS seccion;
DROP TABLE IF EXISTS grupo;
DROP TABLE IF EXISTS personal_proyecto;
DROP TABLE IF EXISTS personal;
DROP TABLE IF EXISTS proyecto;
DROP TABLE IF EXISTS cliente;

CREATE TABLE grupo(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    observaciones TEXT,
    status TINYINT(1) NOT NULL
)ENGINE=InnoDB;


CREATE TABLE seccion(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    observaciones TEXT,
    status TINYINT(1) NOT NULL
)ENGINE=InnoDB;

CREATE TABLE accion(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    seccion_id INT(11) NOT NULL,
    FOREIGN KEY (seccion_id) REFERENCES seccion(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;


CREATE TABLE  accion_grupo(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    accion_id INT(11) NOT NULL,
    grupo_id INT(11) NOT NULL,
    FOREIGN KEY (accion_id) REFERENCES accion(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (grupo_id) REFERENCES grupo(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

CREATE TABLE personal(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(30) NOT NULL,
    apellido_paterno VARCHAR(30) NOT NULL,
    apellido_materno VARCHAR(30),
    telefono VARCHAR(15),
    curp VARCHAR(18),
    direccion VARCHAR(200),
    sueldo VARCHAR(15) NOT NULL,
    especialidad VARCHAR(100),
    correo VARCHAR(500) NOT NULL UNIQUE
)ENGINE=InnoDB;

CREATE TABLE  usuario(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    usuario VARCHAR(10) NOT NULL UNIQUE,
    contrasena VARCHAR(16) NOT NULL,
    personal_id INT(11) NOT NULL,
    grupo_id INT(11) NOT NULL,
    FOREIGN KEY (grupo_id) REFERENCES grupo(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (personal_id) REFERENCES personal(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

CREATE TABLE cliente(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    razon_social VARCHAR(50) NOT NULL UNIQUE,
    telefono_1 VARCHAR(15) NOT NULL UNIQUE,
    telefono_2 VARCHAR(15) UNIQUE,
    telefono_3 VARCHAR(15) UNIQUE,
    rfc VARCHAR(18) NOT NULL UNIQUE,
    calle VARCHAR(50) NOT NULL,
    numero_exterior VARCHAR(10) NOT NULL,
    numero_interior VARCHAR(5),
    colonia VARCHAR(50) NOT NULL,
    codigo_postal VARCHAR(5) NOT NULL,
    forma_pago VARCHAR(100) NOT NULL,
    nombre_contacto VARCHAR(100),
    telefono_contacto VARCHAR(15),
    correo VARCHAR(50) NOT NULL
)ENGINE=InnoDB;

CREATE TABLE proyecto(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(300) NOT NULL UNIQUE,
    tiempo_estimado INT(4),
    costo FLOAT(7,2),
    cliente_id INT(11) NOT NULL,
    FOREIGN KEY (cliente_id) REFERENCES cliente(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

CREATE TABLE personal_proyecto(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    proyecto_id INT(11) NOT NULL,
    personal_id INT(11) NOT NULL,
    FOREIGN KEY (proyecto_id) REFERENCES proyecto(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (personal_id) REFERENCES personal(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

