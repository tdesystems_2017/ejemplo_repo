<form class="form-horizontal alert alert-warning" name="seccionList" id="altaForm" ng-submit="alta(seccionInfo,'seccion','alta_db');" hidden>
<h3 class="text-center">Alta / Seccion</h3>
	<div class="form-group">
		<label for="Descripcion">Descripcion:</label>
		<input type="text" name="descripcion" class="form-control" placeholder="Ingresa Descripcion" ng-model="seccionInfo.descripcion" value="" autofocus required />
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="seccionList.descripcion.$invalid && seccionList.descripcion.$dirty">El campo descripcion esta vacio!</p>
	</div>

	<div class="form-group">
		<label for="Observaciones">Observaciones:</label>
		<input type="text" name="observaciones" class="form-control" placeholder="Ingresa Observaciones" ng-model="seccionInfo.observaciones" value="" autofocus required />
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="seccionList.observaciones.$invalid && seccionList.observaciones.$dirty">El campo observaciones esta vacio!</p>
	</div>
	
	<div class="form-group text-center">
		<label for="Status">Status:</label>
		<label for="" class="radio-inline status">
			<input type="radio" name="grupo_status" value="0" ng-model="seccionInfo.status">Inactivo
		</label>
		<label for="" class="radio-inline status">
			<input type="radio" name="grupo_status" value="1" ng-model="seccionInfo.status">Activo
		</label>
	</div>
	
	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="seccionList.$invalid">Enviar</button>
	</div>
</form>