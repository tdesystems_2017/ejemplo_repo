<?php
	if(!isset($_GET['seccion'])){
		$seccion='';	
	}
	else{
		$seccion = $_GET['seccion'];
	}
	if(!isset($_GET['accion'])){
		$accion='';	
	}
	else{
		$accion = $_GET['accion'];
	}

	if(empty($accion) || empty($seccion)){
		header("Location: http://localhost/angular_crud/");
	}else{
?>		
	<div ng-controller="DbController">
		<nav class="navbar navbar-default">
			<div class="navbar-header">
				<div class="alert alert-default navbar-brand search-box">
					<button class="btn btn-primary" ng-show="show_form" ng-click="formToggle()">Agregar Seccion
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
					</button>
				</div>
				<div class="alert alert-default input-group search-box">
					<span class="input-group-btn">
					<input type="text" class="form-control" placeholder="Busqueda Grupo ..." ng-model="search_query">
					</span>
				</div>
			</div>
		</nav>
		<div class="col-md-6 col-md-offset-3">
			<div ng-include src="'views/<?php echo $seccion ?>/alta.php'"></div>
			<div ng-include src="'views/<?php echo $seccion ?>/modifica.php'"></div>
		</div>
		<div class="clearfix"></div>
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<th>Id</th>
					<th>Descripcion</th>
					<th>Observaciones</th>
					<th>Status</th>
					<th></th>
					<th></th>
				</thead>
				<tbody>
					<tr ng-repeat="registro in registros| filter:search_query">
						<td><span>{{registro.id}}</span></td>
						<td>{{registro.descripcion}}</td>
						<td>{{registro.observaciones}}</td>
						<td>{{registro.status}}</td>
						<td>
						<button class="btn btn-warning" ng-click="editarInfo(registro)" title="Modificar"><span class="glyphicon glyphicon-edit"></span></button>
						</td>
						<td>
						<button class="btn btn-danger" ng-click="elimina(registro,'seccion','elimina_db')" title="Eliminar"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
<?php } ?>