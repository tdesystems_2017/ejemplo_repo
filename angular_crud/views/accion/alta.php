<form class="form-horizontal alert alert-warning" name="accionList" id="altaForm" ng-submit="alta(accionInfo,'accion','alta_db');" hidden>
<h3 class="text-center">Alta / Accion</h3>
	<div class="form-group">
		<label for="Descripcion">Descripcion:</label>
		<input type="text" name="descripcion" class="form-control" placeholder="Ingresa Descripcion" ng-model="accionInfo.descripcion" value="" autofocus required />
	</div>
	<div class="form-group">
		<p class="text-danger" ng-show="accionList.descripcion.$invalid && accionList.descripcion.$dirty">El campo descripcion esta vacio!</p>
	</div>

	<div class="form-group">
		<label for="Seccion">Seccion:</label>
		<select name="seccion_id" ng-model="accionInfo.seccion_id" required>
	    	<option ng-repeat="registro_seccion in registros_seccion" value="{{registro_seccion.id}}">{{registro_seccion.descripcion}}</option>
	    </select>
	</div>

	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="accionList.$invalid">Enviar</button>
	</div>
</form>
