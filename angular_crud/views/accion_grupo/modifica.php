<form class="form-horizontal alert alert-warning" id="modificaForm" ng-submit="modificaInfo(modifica,'accion_grupo','modifica_db')" hidden>
<h3 class="text-center">Modifica / Accion Grupo</h3>

	<div class="form-group">
		<label for="Seccion">Seccion:</label>
		<select name="seccion_id" ng-model="modifica.seccion_id" required>
	    	<option ng-repeat="registro_seccion in registros_seccion" value="{{registro_seccion.id}}" ng-selected="registro_seccion.descripcion == modifica.seccion">{{registro_seccion.descripcion}}</option>
	    </select>
	</div>
	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="accionList.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
	</div>
</form>