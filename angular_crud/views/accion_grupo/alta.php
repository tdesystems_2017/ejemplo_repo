<form class="form-horizontal alert alert-warning" name="accion_grupoList" id="altaForm" ng-submit="alta(accion_grupoInfo,'accion_grupo','alta_db');" hidden>
<h3 class="text-center">Alta / Accion Grupo</h3>

	<div class="form-group">
		<label for="Accion">Accion:</label>
		<select name="accion_id" ng-model="accion_grupoInfo.accion_id" required>
	    	<option ng-repeat="registro_accion in registros_accion" value="{{registro_accion.id}}">{{registro_accion.descripcion}}</option>
	    </select>
	</div>

	<div class="form-group">
		<label for="Grupo">Grupo:</label>
		<select name="grupo_id" ng-model="accion_grupoInfo.grupo_id" required>
	    	<option ng-repeat="registro_grupo in registros_grupo" value="{{registro_grupo.id}}">{{registro_grupo.descripcion}}</option>
	    </select>
	</div>

	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="accion_grupoList.$invalid">Enviar</button>
	</div>
</form>
