<form class="form-horizontal alert alert-warning" name="grupoList" id="altaForm" ng-submit="alta(grupoInfo,'grupo','alta_db');" hidden>

<h3 class="text-center">Alta / Grupo</h3>
	<div class="form-group">
		<label for="Descripcion">Descripcion</label>
		<input type="text" name="descripcion" class="form-control" placeholder="Ingresa Descripcion" ng-model="grupoInfo.descripcion" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.descripcion.$invalid && grupoList.descripcion.$dirty">El campo descripcion esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Observaciones">observaciones</label>
		<input type="text" name="observaciones" class="form-control" placeholder="Ingresa Observaciones" ng-model="grupoInfo.observaciones" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="grupoList.observaciones.$invalid && grupoList.observaciones.$dirty">El campo observaciones esta vacio</p>
	</div>

	<div class="form-group text-center">
		<label for="Status">Status</label>
		<label for="" class="radio-inline status">
			<input type="radio" name="grupo_status" value="0" ng-model="grupoInfo.status">Inactivo
		</label>

		<label for="" class="radio-inline status">
			<input type="radio" name="grupo_status" value="1" ng-model="grupoInfo.status">Activo
		</label>
	</div>

	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="grupoList.$invalid">Enviar
		</button>
	</div>
</form>