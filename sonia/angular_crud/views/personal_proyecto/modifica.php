<form class="form-horizontal alert alert-warning" id="modificaForm" ng-submit="modificaInfo(modifica,'personal_proyecto','modifica_db')" hidden>
<h3 class="text-center">Modifica / Personal Proyecto</h3>
 		<div class="form-group">
 				<label for="Proyecto Id">Proyecto Id:</label>
 				<input type="text" class="form-control" ng-model="modifica.proyecto_id" value="{{modifica.proyecto_id}}">
 		</div>

 		<div class="form-group">
 				<label for="Personal Id">Personal Id:</label>
 				<input type="text" class="form-control" ng-model="modifica.personal_id" value="{{modifica.personal_id}}">
 		</div>

 		
 		<div class="form-group text-center">
 				<button class="btn btn-warnig" ng-disabled="PersonalproyectoList.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
 		</div>
</form>