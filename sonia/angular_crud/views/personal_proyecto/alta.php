<form class="form-horizontal alert alert-warning" name="personalproyectoList" id="altaForm" ng-submit="alta(personalproyectoInfo,'personal_proyecto','alta_db');" hidden>

<h3 class="text-center">Alta / Personal Proyecto</h3>
	<div class="form-group">
		<label for="Id Proyecto">Id Proyecto</label>
		<input type="text" name="proyecto_id" class="form-control" placeholder="Id Proyecto" ng-model="personalproyectoInfo.proyecto_id" value="" autofocus required>
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalproyectoList.proyecto_id.$invalid && personalproyectoList.proyecto_id.$dirty">El campo Id del proyecto esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Id Personal">Id Personal</label>
		<input type="text" name="personal_id" class="form-control" placeholder="Personal Id" ng-model="personalproyectoInfo.personal_id" value="" autofocus required>
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalproyectoList.personal_id.$invalid && personalproyectoList.personal_id.$dirty">El campo Id Personal esta vacio</p>
	</div>

	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="personalproyectoList.$invalid">Enviar
		</button>
	</div>
</form>