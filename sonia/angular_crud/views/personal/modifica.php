<form class="form-horizontal alert alert-warning" id="modificaForm" ng-submit="modificaInfo(modifica,'personal','modifica_db')" hidden>
<h3 class="text-center">Modifica / Personal</h3>

 		<div class="form-group">
 				<label for="Nombre">Nombre:</label>
 				<input type="text" class="form-control" ng-model="modifica.nombre" value="{{modifica.nombre}}">
 		</div>

 		<div class="form-group">
 				<label for="Apellido Paterno">Apellido Paterno:</label>
 				<input type="text" class="form-control" ng-model="modifica.apellido_paterno" value="{{modifica.apellido_paterno}}">
 		</div>

 		<div class="form-group">
 				<label for="Apellido Materno">Apellido Materno:</label>
 				<input type="text" class="form-control" ng-model="modifica.apellido_materno" value="{{modifica.apellido_materno}}">
 		</div>


 			<div class="form-group">
 				<label for="Telefono">Telefono:</label>
 				<input type="text" class="form-control" ng-model="modifica.telefono" value="{{modifica.telefono}}">
 		</div>


 		<div class="form-group">
 				<label for="CURP">CURP:</label>
 				<input type="text" class="form-control" ng-model="modifica.curp" value="{{modifica.curp}}">
 		</div>


 		<div class="form-group">
 				<label for="Direccion">Direccion:</label>
 				<input type="text" class="form-control" ng-model="modifica.direccion" value="{{modifica.direccion}}">
 		</div>
 		


 			<div class="form-group">
 				<label for="Sueldo">Sueldo:</label>
 				<input type="text" class="form-control" ng-model="modifica.sueldo" value="{{modifica.sueldo}}">
 		</div>


 		<div class="form-group">
 				<label for="Especialidad">Especialidad:</label>
 				<input type="text" class="form-control" ng-model="modifica.especialidad" value="{{modifica.especialidad}}">
 		</div>


 		<div class="form-group">
 				<label for="Correo">Correo:</label>
 				<input type="text" class="form-control" ng-model="modifica.correo" value="{{modifica.correo}}">
 		</div>




 		<div class="form-group text-center">
 				<button class="btn btn-warnig" ng-disabled="personalList.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
 		</div>
</form>