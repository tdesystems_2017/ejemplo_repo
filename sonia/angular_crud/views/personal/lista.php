<?php
if(!isset($_GET['seccion'])){
    $seccion='';
}
else{
    $seccion = $_GET['seccion'];
}
if(!isset($_GET['accion'])) {
    $accion='';
}else{
    $accion = $_GET['accion'];
}
if(empty($accion) || empty($seccion)){
   header("Location: http://localhost/ejemplo_repo/sonia/angular_crud");
}else{

?>
<div ng-controller="DbController">
   
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <div class="alert alert-default navbar-brand search-box">
              <button class="btn btn-primary" ng-show="show_form" ng-click="formToggle()">Agregar Personal
                <span class="glyphicon glyphicon-plus" aria-hidden="true">
                </span>
              </button>
            </div>
            <div class="alert alert-default input-group search-box">
              <span class="input-group-btn">
                <input type="text" class="form-control" placeholder="Busqueda Personal..." ng-model="search_query">
              </span>
            </div>
          </div>
        </nav>
        <div class="col-md-6 col-md-offset-3">
            <div ng-include src="'views/<?php echo $seccion; ?>/alta.php'"></div>

            <div ng-include src="'views/<?php echo $seccion; ?>/modifica.php'"></div>
        </div>
        <div class="clearfix"></div>
          <div class="table-responsive">
	           <table class="table table-hover">
		            <thead>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Apeido Paterno</th>
                    <th>Apeido Materno</th>
                    <th>Telefono</th>
                    <th>CURP</th>
                    <th>Direccion</th>
                    <th>Sueldo</th>
                    <th>Especialidad</th>
                    <th>Correo</th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    <tr ng-repeat="registro in registros | filter:search_query">
                    <td>
                        <span>{{registro.id}}</span>
                    </td>
                    <td>
                        <span>{{registro.nombre}}</span>
                    </td>
                    <td>
                        <span>{{registro.apellido_paterno}}</span>
                    </td>
                    <td>
                        <span>{{registro.apellido_materno}}</span>
                    </td>
                     <td>
                        <span>{{registro.telefono}}</span>
                    </td>
                     <td>
                        <span>{{registro.curp}}</span>
                    </td>
                     <td>
                        <span>{{registro.direccion}}</span>
                    </td>
                     <td>
                        <span>{{registro.sueldo}}</span>
                    </td>
                     <td>
                        <span>{{registro.especialidad}}</span>
                    </td>
                     <td>
                        <span>{{registro.correo}}</span>
                    </td>
                    <td>
                        <button class="btn btn-warning" ng-click="editarInfo(registro,'personal','modifica_db')" title="Modificar"><span class="glyphicon glyphicon-edit"></span>
                        </button>
                    </td>
                    <td>
                        <button class="btn btn-danger" ng-click="elimina(registro,'personal','elimina_db')" title="Eliminar"><span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </td>
                    </tr>
                </tbody>
              </table>
          </div>
    </div>
<?php } ?>