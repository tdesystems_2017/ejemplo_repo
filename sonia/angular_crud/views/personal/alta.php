<form class="form-horizontal alert alert-warning" name="personalList" id="altaForm" ng-submit="alta(personalInfo,'personal','alta_db');" hidden>

<h3 class="text-center">Alta / Personal</h3>
	<div class="form-group">
		<label for="Nombre">Nombre</label>
		<input type="text" name="nombre" class="form-control" placeholder="Ingresa tu nombre" ng-model="personalInfo.nombre" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalList.nombre.$invalid && personalList.nombre.$dirty">El campo nombre esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Apellido Paterno">Apellido Paterno</label>
		<input type="text" name="apellido_paterno" class="form-control" placeholder="Ingresa tu apellido paterno" ng-model="personalInfo.apellido_paterno" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalList.apellido_paterno.$invalid && personalList.apellido_paterno.$dirty">El campo Apellido Paterno esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Apellido Materno">Apellido Materno</label>
		<input type="text" name="apellido_materno" class="form-control" placeholder="Ingresa tu apellido materno" ng-model="personalInfo.apellido_materno" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalList.apellido_materno.$invalid && personalList.apellido_materno.$dirty">El campo Apellido Materno esta vacio</p>
	</div>

	

	<div class="form-group">
		<label for="Telefono">Telefono</label>
		<input type="text" name="telefono" class="form-control" placeholder="Ingresa tu telefono" ng-model="personalInfo.telefono" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalList.telefono.$invalid && personalList.telefono.$dirty">El campo telefono esta vacio</p>
	</div>


	<div class="form-group">
		<label for="CURP">CURP</label>
		<input type="text" name="curp" class="form-control" placeholder="Ingresa tu CURP" ng-model="personalInfo.curp" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalList.curp.$invalid && personalList.curp.$dirty">El campo CURP esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Direccion">Direccion </label>
		<input type="text" name="direccion" class="form-control" placeholder="Ingresa tu direccion" ng-model="personalInfo.direccion" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalList.direccion.$invalid && personalList.direccion.$dirty">El campo direccion esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Sueldo">Sueldo</label>
		<input type="text" name="sueldo" class="form-control" placeholder="Ingresa tu sueldo" ng-model="personalInfo.sueldo" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalList.sueldo.$invalid && personalList.sueldo.$dirty">El campo sueldo esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Especialidad">Especialidad</label>
		<input type="text" name="especialidad" class="form-control" placeholder="Ingresa tu especialidad" ng-model="personalInfo.especialidad" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalList.especialidad.$invalid && personalList.especialidad.$dirty">El campo especialidad esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Correo">Correo</label>
		<input type="text" name="correo" class="form-control" placeholder="Ingresa tu correo" ng-model="personalInfo.correo" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="personalList.correo.$invalid && personalList.correo.$dirty">El campo especialidad esta vacio</p>
	</div>



	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="personalList.$invalid">Enviar
		</button>
	</div>
</form>