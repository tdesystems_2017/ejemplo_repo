<form class="form-horizontal alert alert-warning" name="clienteList" id="altaForm" ng-submit="alta(clienteInfo,'cliente','alta_db');" hidden>

<h3 class="text-center">Alta / Cliente</h3>

	<div class="form-group">
		<label for="Razon social">Razon social</label>
		<input type="text" name="razon_social" class="form-control" placeholder="Ingresa la Razon social" ng-model="clienteInfo.razon_social" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.razon_social.$invalid && clienteList.razon_social.$dirty">El campo Razon social esta vacio</p>
	</div>

  	<div class="form-group">
		<label for="Telefono1">Telefono 1</label>
		<input type="text" name="telefono_1" class="form-control" placeholder="Ingresa numero de telefono" ng-model="clienteInfo.telefono_1" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.telefono_1.$invalid && clienteList.telefono_1.$dirty">El campo Telefono 1 esta vacio</p>
	</div>

	<div class="form-group">
		<label for="Telefono2">Telefono 2</label>
		<input type="text" name="telefono_2" class="form-control" placeholder="Ingresa numero de telefono 2" ng-model="clienteInfo.telefono_2" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.telefono_2.$invalid && clienteList.telefono_2.$dirty">El campo Telefono 2 esta vacio</p>
	</div>


		<div class="form-group">
		<label for="Telefono3">Telefono 3</label>
		<input type="text" name="telefono_3" class="form-control" placeholder="Ingresa numero de telefono 3" ng-model="clienteInfo.telefono_3" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.telefono_3.$invalid && clienteList.telefono_3.$dirty">El campo Telefono 3 esta vacio</p>
	</div>

	<div class="form-group">
		<label for="RFC">RFC</label>
		<input type="text" name="rfc" class="form-control" placeholder="Ingresa tu RFC" ng-model="clienteInfo.rfc" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.rfc.$invalid && clienteList.rfc.$dirty">El campo RFC esta vacio</p>
	</div>


  <div class="form-group">
		<label for="Calle">Calle</label>
		<input type="text" name="calle" class="form-control" placeholder="Ingresa tu Calle" ng-model="clienteInfo.calle" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.calle.$invalid && clienteList.calle.$dirty">El campo Calle esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Numero Exterior">Numero Exterior</label>
		<input type="text" name="numero_exterior" class="form-control" placeholder="Ingresa tu Numero Exterior" ng-model="clienteInfo.numero_exterior" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.numero_exterior.$invalid && clienteList.numero_exterior.$dirty">El campo Numero Exterior esta vacio</p>
	</div>

  <div class="form-group">
		<label for="Numero Interior">Numero Interior</label>
		<input type="text" name="numero_interior" class="form-control" placeholder="Ingresa tu Numero Interior" ng-model="clienteInfo.numero_interior" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.numero_interior.$invalid && clienteList.numero_interior.$dirty">El campo Numero Interior esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Colonia">Colonia</label>
		<input type="text" name="colonia" class="form-control" placeholder="Ingresa tu Colonia" ng-model="clienteInfo.colonia" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.colonia.$invalid && clienteList.colonia.$dirty">El campo Colonia esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Codigo Postal">Codigo Postal</label>
		<input type="text" name="codigo_postal" class="form-control" placeholder="Ingresa tu Codigo Postal" ng-model="clienteInfo.codigo_postal" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.codigo_postal.$invalid && clienteList.codigo_postal.$dirty">El campo Codigo Postal esta vacio</p>
	</div>



	<div class="form-group">
		<label for="Forma de Pago">Forma de Pago</label>
		<input type="text" name="forma_pago" class="form-control" placeholder="Ingresa Forma de pago" ng-model="clienteInfo.forma_pago" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.forma_pago.$invalid && clienteList.forma_pago.$dirty">El campo Forma de Pago esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Forma de Contacto">Nombre de Contacto</label>
		<input type="text" name="nombre_contacto" class="form-control" placeholder="Ingresa Nombre de Contacto" ng-model="clienteInfo.nombre_contacto" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.nombre_contacto.$invalid && clienteList.nombre_contacto.$dirty">El campo Nombre de Contacto esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Telefono de Contacto">Telefono de Contacto</label>
		<input type="text" name="telefono_contacto" class="form-control" placeholder="Ingresa Telefono de Contacto" ng-model="clienteInfo.telefono_contacto" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.telefono_contacto.$invalid && clienteList.telefono_contacto.$dirty">El campo Telefono de Contacto esta vacio</p>
	</div>



	<div class="form-group">
		<label for="Correo">Correo</label>
		<input type="text" name="correo" class="form-control" placeholder="Ingresa Correo" ng-model="clienteInfo.correo" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="clienteList.correo.$invalid && clienteList.correo.$dirty">El campo Correo esta vacio</p>
	</div>
	

	

	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="clienteList.$invalid">Enviar
		</button>
	</div>
</form>