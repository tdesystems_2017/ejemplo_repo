<?php
if(!isset($_GET['seccion'])){
    $seccion='';
}
else{
    $seccion = $_GET['seccion'];
}
if(!isset($_GET['accion'])) {
    $accion='';
}else{
    $accion = $_GET['accion'];
}
if(empty($accion) || empty($seccion)){
   header("Location: http://localhost/ejemplo_repo/sonia/angular_crud");
}else{

?>
<div ng-controller="DbController">
   
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <div class="alert alert-default navbar-brand search-box">
              <button class="btn btn-primary" ng-show="show_form" ng-click="formToggle()">Agregar Cliente
                <span class="glyphicon glyphicon-plus" aria-hidden="true">
                </span>
              </button>
            </div>
            <div class="alert alert-default input-group search-box">
              <span class="input-group-btn">
                <input type="text" class="form-control" placeholder="Busqueda Cliente..." ng-model="search_query">
              </span>
            </div>
          </div>
        </nav>
        <div class="col-md-6 col-md-offset-3">
            <div ng-include src="'views/<?php echo $seccion; ?>/alta.php'"></div>

            <div ng-include src="'views/<?php echo $seccion; ?>/modifica.php'"></div>
        </div>
        <div class="clearfix"></div>
          <div class="table-responsive">
	           <table class="table table-hover">
		            <thead>
                    <th>Id</th>
                    <th>Razon Social</th>
                    <th>Telefono 1</th>
                    <th>Telefono 2</th>
                    <th>Telefono 3</th>
                    <th>RFC</th>
                    <th>Calle</th>
                    <th>Numero Exterior</th>
                    <th>Numero Interior</th>
                    <th>Colonia</th>
                    <th>Codigo Postal</th>
                    <th>Forma de pago</th>
                    <th>Nombre de Contacto</th>
                    <th>Telefono de Contacto</th>
                    <th>Correo</th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    <tr ng-repeat="registro in registros | filter:search_query">
                    <td>
                        <span>{{registro.id}}</span>
                    </td>
                    <td>
                        <span>{{registro.razon_social}}</span>
                    </td>
                    <td>
                        <span>{{registro.telefono1}}</span>
                    </td>
                    <td>
                        <span>{{registro.telefono2}}</span>
                    </td>
                    <td>
                        <span>{{registro.telefono3}}</span>
                    </td>
                    <td>
                        <span>{{registro.rfc}}</span>
                    </td>
                    <td>
                        <span>{{registro.calle}}</span>
                    </td>
                    <td>
                        <span>{{registro.numero_exterior}}</span>
                    </td>
                    <td>
                        <span>{{registro.numero_interior}}</span>
                    </td>
                    <td>
                        <span>{{registro.colonia}}</span>
                    </td>
                    <td>
                        <span>{{registro.codigo_postal}}</span>
                    </td>
                    <td>
                        <span>{{registro.forma_pago}}</span>
                    </td>
                    <td>
                        <span>{{registro.nombre_contacto}}</span>
                    </td>
                    <td>
                        <span>{{registro.telefono_contacto}}</span>
                    </td>
                    <td>
                        <span>{{registro.correo}}</span>
                    </td>

                    <td>
                        <button class="btn btn-warning" ng-click="editarInfo(registro)" title="Modificar"><span class="glyphicon glyphicon-edit"></span>
                        </button>
                    </td>
                    <td>
                        <button class="btn btn-danger" ng-click="elimina(registro,'cliente','elimina_db')" title="Eliminar"><span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </td>
                    </tr>
                </tbody>
              </table>
          </div>
    </div>
<?php } ?>