<form class="form-horizontal alert alert-warning" id="modificaForm" ng-submit="modificaInfo(modifica,'cliente','modifica_db')" hidden>
<h3 class="text-center">Modifica / Cliente</h3>
 		<div class="form-group">
 				<label for="Razon Social">Razon Social:</label>
 				<input type="text" class="form-control" ng-model="modifica.razon_social" value="{{modifica.razon_social}}">
 		</div>

 		<div class="form-group">
 				<label for="Telefono1">Telefono 1:</label>
 				<input type="text" class="form-control" ng-model="modifica.telefono_1" value="{{modifica.telefono_1}}">
 		</div>

 		<div class="form-group">
 				<label for="Telefono2">Telefono 2:</label>
 				<input type="text" class="form-control" ng-model="modifica.telefono_2" value="{{modifica.telefono_2}}">
 		</div>

 		<div class="form-group">
 				<label for="Telefono3">Telefono 3:</label>
 				<input type="text" class="form-control" ng-model="modifica.telefono_3" value="{{modifica.telefono_3}}">
 		</div>


 		<div class="form-group">
 				<label for="RFC">RFC:</label>
 				<input type="text" class="form-control" ng-model="modifica.rfc" value="{{modifica.rfc}}">
 		</div>

 		<div class="form-group">
 				<label for="Calle">Calle:</label>
 				<input type="text" class="form-control" ng-model="modifica.calle" value="{{modifica.calle}}">
 		</div>

 		<div class="form-group">
 				<label for="Numero Exterior">Numero Exterior:</label>
 				<input type="text" class="form-control" ng-model="modifica.numero_exterior" value="{{modifica.numero_exterior}}">
 		</div>

 		<div class="form-group">
 				<label for="Numero Interior">Numero Interior:</label>
 				<input type="text" class="form-control" ng-model="modifica.numero_interior" value="{{modifica.numero_interior}}">
 		</div>

 		<div class="form-group">
 				<label for="Colonia">Colonia:</label>
 				<input type="text" class="form-control" ng-model="modifica.colonia" value="{{modifica.colonia}}">
 		</div>


 		<div class="form-group">
 				<label for="Codigo Postal">Codigo Postal:</label>
 				<input type="text" class="form-control" ng-model="modifica.codigo_postal" value="{{modifica.codigo_postal}}">
 		</div>


 		<div class="form-group">
 				<label for="Forma de Pago">Forma de Pago:</label>
 				<input type="text" class="form-control" ng-model="modifica.forma_pago" value="{{modifica.forma_pago}}">
 		</div>


 		<div class="form-group">
 				<label for="Nombre de Contacto">Nombre de Contacto:</label>
 				<input type="text" class="form-control" ng-model="modifica.nombre_contacto" value="{{modifica.nombre_contacto}}">
 		</div>


 		<div class="form-group">
 				<label for="Telefono de Contacto">Telefono de Contacto:</label>
 				<input type="text" class="form-control" ng-model="modifica.telefono_contacto" value="{{modifica.telefono_contacto}}">
 		</div>


 		<div class="form-group">
 				<label for="Correo">Correo:</label>
 				<input type="text" class="form-control" ng-model="modifica.correo" value="{{modifica.correo}}">
 		</div>

 		<div class="form-group text-center">
 				<button class="btn btn-warnig" ng-disabled="clienteList.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
 		</div>
</form>