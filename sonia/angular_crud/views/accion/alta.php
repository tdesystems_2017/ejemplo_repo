<form class="form-horizontal alert alert-warning" name="accionList" id="altaForm" ng-submit="alta(accionInfo,'accion','alta_db');" hidden>

<h3 class="text-center">Alta / Accion</h3>
	<div class="form-group">
		<label for="Descripcion">Descripcion</label>
		<input type="text" name="descripcion" class="form-control" placeholder="Ingresa Descripcion" ng-model="accionInfo.descripcion" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="accionList.descripcion.$invalid && accionList.descripcion.$dirty">El campo descripcion esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Seccion_id">Seccion_id</label>
		<input type="text" name="seccion_id" class="form-control" placeholder="Ingresa Seccion_id" ng-model="accionInfo.seccion_id" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="accionList.seccion_id.$invalid && accionList.seccion_id.$dirty">El campo Seccion_id esta vacio</p>
	</div>

	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="accionList.$invalid">Enviar
		</button>
	</div>
</form>