<form class="form-horizontal alert alert-warning" id="modificaForm" ng-submit="modificaInfo(modifica,'accion','modifica_db')" hidden>
<h3 class="text-center">Modifica / accion</h3>
 		<div class="form-group">
 				<label for="Descripcion">Descripcion:</label>
 				<input type="text" class="form-control" ng-model="modifica.descripcion" value="{{modifica.descripcion}}">
 		</div>
 		<div class="form-group">
 				<label for="Seccion_id">Seccion_id:</label>
 				<input type="text" class="form-control" ng-model="modifica.seccion_id" value="{{modifica.seccion_id}}">
 		</div>
 	
 		<div class="form-group text-center">
 				<button class="btn btn-warnig" ng-disabled="accionList.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
 		</div>
</form>