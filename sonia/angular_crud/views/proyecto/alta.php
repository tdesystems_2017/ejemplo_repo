<form class="form-horizontal alert alert-warning" name="proyectoList" id="altaForm" ng-submit="alta(proyectoInfo,'proyecto','alta_db');" hidden>

<h3 class="text-center">Alta / Proyecto</h3>

	<div class="form-group">
		<label for="Nombre">Nombre</label>
		<input type="text" name="nombre" class="form-control" placeholder="Nombre" ng-model="proyectoInfo.nombre" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="proyectoList.nombre.$invalid && proyectoList.nombre.$dirty">El campo Nombre esta vacio</p>
	</div>

  <div class="form-group">
		<label for="Tiempo Estimado">Tiempo Estimado</label>
		<input type="text" name="tiempo_estimado" class="form-control" placeholder="Ingresa el Tiempo Estimado" ng-model="proyectoInfo.tiempo_estimado" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="proyectoList.tiempo_estimado.$invalid && proyectoList.tiempo_estimado.$dirty">El campo Tiempo Estimado esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Costo">Costo</label>
		<input type="text" name="costo" class="form-control" placeholder="Costo" ng-model="proyectoInfo.costo" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="proyectoList.costo.$invalid && proyectoList.costo.$dirty">El campo Costo esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Cliente Id">Cliente Id</label>
		<input type="text" name="cliente_id" class="form-control" placeholder="Cliente Id" ng-model="proyectoInfo.cliente_id" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="proyectoList.cliente_id.$invalid && proyectoList.cliente_id.$dirty">Cliente Id esta vacio</p>
	</div>


	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="proyectoList.$invalid">Enviar
		</button>
	</div>
</form>