<form class="form-horizontal alert alert-warning" id="modificaForm" ng-submit="modificaInfo(modifica,'proyecto','modifica_db')" hidden>
<h3 class="text-center">Modifica / Proyecto</h3>
 		<div class="form-group">
 				<label for="Nombre">Nombre:</label>
 				<input type="text" class="form-control" ng-model="modifica.nombre" value="{{modifica.nombre}}">
 		</div>

 		<div class="form-group">
 				<label for="Tiempo Estimado">Tiempo Estimado:</label>
 				<input type="text" class="form-control" ng-model="modifica.tiempo_estimado" value="{{modifica.tiempo_estimado}}">
 		</div>

 		<div class="form-group">
 				<label for="Costo">Costo:</label>
 				<input type="text" class="form-control" ng-model="modifica.costo" value="{{modifica.costo}}">
 		</div>

 		<div class="form-group">
 				<label for="Cliente Id">Cliente Id:</label>
 				<input type="text" class="form-control" ng-model="modifica.cliente_id" value="{{modifica.cliente_id}}">
 		</div>

 		
 		<div class="form-group text-center">
 				<button class="btn btn-warnig" ng-disabled="usuarioList.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
 		</div>
</form>