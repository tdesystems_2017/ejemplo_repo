<?php
if(!isset($_GET['seccion'])){
    $seccion='';
}
else{
    $seccion = $_GET['seccion'];
}
if(!isset($_GET['accion'])) {
    $accion='';
}else{
    $accion = $_GET['accion'];
}
if(empty($accion) || empty($seccion)){
   header("Location: http://localhost/ejemplo_repo/sonia/angular_crud");
}else{

?>
<div ng-controller="DbController">
   
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <div class="alert alert-default navbar-brand search-box">
              <button class="btn btn-primary" ng-show="show_form" ng-click="formToggle()">Agregar Usuario
                <span class="glyphicon glyphicon-plus" aria-hidden="true">
                </span>
              </button>
            </div>
            <div class="alert alert-default input-group search-box">
              <span class="input-group-btn">
                <input type="text" class="form-control" placeholder="Busqueda Usuario..." ng-model="search_query">
              </span>
            </div>
          </div>
        </nav>
        <div class="col-md-6 col-md-offset-3">
            <div ng-include src="'views/<?php echo $seccion; ?>/alta.php'"></div>

            <div ng-include src="'views/<?php echo $seccion; ?>/modifica.php'"></div>
        </div>
        <div class="clearfix"></div>
          <div class="table-responsive">
	           <table class="table table-hover">
		            <thead>
                    <th>Id</th>
                    <th>Usuario</th>
                    <th>Contrasena</th>
                    <th>Personal Id</th>
                    <th>Grupo Id</th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    <tr ng-repeat="registro in registros | filter:search_query">
                    <td>
                        <span>{{registro.id}}</span>
                    </td>
                    <td>
                        <span>{{registro.usuario}}</span>
                    </td>
                    <td>
                        <span>{{registro.contrasena}}</span>
                    </td>
                    <td>
                        <span>{{registro.personal_id}}</span>
                    </td>
                    <td>
                        <span>{{registro.grupo_id}}</span>
                    </td>
                    <td>
                        <button class="btn btn-warning" ng-click="editarInfo(registro)" title="Modificar"><span class="glyphicon glyphicon-edit"></span>
                        </button>
                    </td>
                    <td>
                        <button class="btn btn-danger" ng-click="elimina(registro,'usuario','elimina_db')" title="Eliminar"><span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </td>
                    </tr>
                </tbody>
              </table>
          </div>
    </div>
<?php } ?>