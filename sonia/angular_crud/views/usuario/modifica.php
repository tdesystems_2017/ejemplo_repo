<form class="form-horizontal alert alert-warning" id="modificaForm" ng-submit="modificaInfo(modifica,'usuario','modifica_db')" hidden>
<h3 class="text-center">Modifica / Usuario</h3>
 		<div class="form-group">
 				<label for="Usuario">Usuario:</label>
 				<input type="text" class="form-control" ng-model="modifica.usuario" value="{{modifica.usuario}}">
 		</div>

 		<div class="form-group">
 				<label for="Contrasena">Contrasena:</label>
 				<input type="text" class="form-control" ng-model="modifica.contrasena" value="{{modifica.contrasena}}">
 		</div>

 		<div class="form-group">
 				<label for="Personal Id">Personal Id:</label>
 				<input type="text" class="form-control" ng-model="modifica.personal_id" value="{{modifica.personal_id}}">
 		</div>

 		<div class="form-group">
 				<label for="Grupo Id">Grupo Id:</label>
 				<input type="text" class="form-control" ng-model="modifica.grupo_id" value="{{modifica.grupo_id}}">
 		</div>

 		
 		<div class="form-group text-center">
 				<button class="btn btn-warnig" ng-disabled="usuarioList.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
 		</div>
</form>