<form class="form-horizontal alert alert-warning" name="usuarioList" id="altaForm" ng-submit="alta(usuarioInfo,'usuario','alta_db');" hidden>

<h3 class="text-center">Alta / Usuario</h3>

	<div class="form-group">
		<label for="Usuario">Usuario</label>
		<input type="text" name="usuario" class="form-control" placeholder="Usuario" ng-model="usuarioInfo.usuario" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="usuarioList.usuario.$invalid && usuarioList.usuario.$dirty">El campo Usuario esta vacio</p>
	</div>

  <div class="form-group">
		<label for="Contrasena">Contrasena</label>
		<input type="text" name="contrasena" class="form-control" placeholder="Ingresa la Contrasena" ng-model="usuarioInfo.contrasena" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="usuarioList.contrasena.$invalid && usuarioList.contrasena.$dirty">El campo Contrasena esta vacio</p>
	</div>

	<div class="form-group">
		<label for="Personal Id">Personal Id</label>
		<input type="text" name="personal_id" class="form-control" placeholder="Personal Id" ng-model="usuarioInfo.personal_id" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="usuarioList.personal_id.$invalid && usuarioList.personal_id.$dirty">Personal Id esta vacio</p>
	</div>


		<div class="form-group">
		<label for="Grupo Id">Grupo Id</label>
		<input type="text" name="grupo_id" class="form-control" placeholder="Grupo Id" ng-model="usuarioInfo.grupo_id" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="usuarioList.grupo_id.$invalid && usuarioList.grupo_id.$dirty">Grupo Id esta vacio</p>
	</div>

	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="usuarioList.$invalid">Enviar
		</button>
	</div>
</form>