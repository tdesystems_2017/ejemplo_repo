<form class="form-horizontal alert alert-warning" name="accion_grupoList" id="altaForm" ng-submit="alta(accion_grupoInfo,'accion_grupo','alta_db');" hidden>

<h3 class="text-center">Alta / Accion Grupo</h3>
	<div class="form-group">
		<label for="Accion Id">Accion Id</label>
		<input type="text" name="accion_id" class="form-control" placeholder="Ingresa Accion Id" ng-model="accion_grupoInfo.accion_id" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="accion_grupoList.accion_id.$invalid && accion_grupoList.accion_id.$dirty">El campo Accion Id esta vacio</p>
	</div>


	<div class="form-group">
		<label for="Grupo Id">Grupo Id</label>
		<input type="text" name="grupo_id" class="form-control" placeholder="Ingresa Grupo Id" ng-model="accion_grupoInfo.grupo_id" value="" autofocus required>	
	</div>
	
	<div class="form-group">
		<p class="text-danger" ng-show="accion_grupoList.grupo_id.$invalid && accion_grupoList.grupo_id.$dirty">El campo Grupo Id esta vacio</p>
	</div>

	
	<div class="form-group text-center">
		<button class="btn btn-warning" ng-disabled="accion_grupoList.$invalid">Enviar
		</button>
	</div>
</form>