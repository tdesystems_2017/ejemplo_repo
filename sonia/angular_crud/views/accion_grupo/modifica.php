<form class="form-horizontal alert alert-warning" id="modificaForm" ng-submit="modificaInfo(modifica,'accion_grupo','modifica_db')" hidden>
<h3 class="text-center">Modifica /Accion Grupo</h3>
 		<div class="form-group">
 				<label for="Accion Id">Accion Id:</label>
 				<input type="text" class="form-control" ng-model="modifica.accion_id" value="{{modifica.accion_id}}">
 		</div>
 		<div class="form-group">
 				<label for="Grupo Id">Grupo Id:</label>
 				<input type="text" class="form-control" ng-model="modifica.grupo_id" value="{{modifica.grupo_id}}">
 		</div>
 		
 		<div class="form-group text-center">
 				<button class="btn btn-warnig" ng-disabled="accion_grupoList.$invalid" ng-click="modificaMsg(modifica.id)">Enviar</button>
 		</div>
</form>