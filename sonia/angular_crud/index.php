<?php
if(!isset($_GET['seccion'])){
    $seccion='';
}
else{
    $seccion = $_GET['seccion'];
}
if(!isset($_GET['accion'])) {
    $accion='';
}else{
    $accion = $_GET['accion'];
}
if(($seccion==''||$seccion=="sesion") && ($accion==''||$accion=="login"))
?>

<!DOCTYPE html>
<html ng-app="crudApp">
<head>
    <title>Sistema Agro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="js/jQuery/jquery.min.js"></script>
    <script src="lib/angular/angular.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container wrapper" ng-controller="DbController">
        <h1 class="text-center">Sistema Agro</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel with-nav-tabs panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li><a href="index.php">Home</a></li>

                            <li><a href="index.php?seccion=grupo&accion=lista">Grupo</a></li>

                            <li><a href="index.php?seccion=seccion&accion=lista">Seccion</a></li> 

                            <li><a href="index.php?seccion=accion&accion=lista">Accion</a></li> 


                            <li><a href="index.php?seccion=accion_grupo&accion=lista">Accion Grupo</a></li>


                            <li><a href="index.php?seccion=personal&accion=lista">Personal</a></li>


                            <li><a href="index.php?seccion=usuario&accion=lista">Usuario</a></li> 


                            <li><a href="index.php?seccion=cliente&accion=lista">Cliente</a></li>


                            <li><a href="index.php?seccion=proyecto&accion=lista">Proyecto</a></li> 


                             <li><a href="index.php?seccion=personal_proyecto&accion=lista">Personal Proyecto</a></li> 
                        </ul>  
                    </div>
                    <div class="panel-body">
                       <div class="tab-pane fade in active" id="home">
                       <?php
                       if (!empty($seccion)&&!empty($accion)){
                        ?>
                           <h1 class="text-center">
                              <?php echo $seccion.' / '.$accion;
                              ?>
                           </h1>
                        <?php
                               include('./views/'.$seccion.'/'.$accion.'.php');
                           }
                        ?>   
                       </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
if($seccion !== 'sesion'){
?>
<script src="js/angular-script.js"></script>
<?php
}
?>
</body>
</html>