<?php

  require_once('./lib/nusoap.php');
  require_once('./controladores/controlador_proyecto.php');

  $server = new nusoap_server();
  $ns = 'http://localhost/ejemplo_repo/sonia/angular_crud/servidor/nusoap';
  $server->configureWSDL('Server Proyecto',$ns);
  $server->wsdl->schemaTargetNamespace=$ns;


//Funcion Alta
  $server->register("alta_db",
  	array("registro"=>"xsd:Array","tabla"=>"xsd:string"),
  	array("return"=>"xsd:string"));
  
//Funcion Elimina
  $server->register("elimina_db", 
  array("registro"=>"xsd:string","tabla"=>"xsd:string"),
  array("return"=>"xsd:string"));

//Funcion Modifica
  $server->register("modifica_db", 
  array("registro"=>"xsd:Array","id"=>"xsd:string","tabla"=>"xsd:string"),
  array("return"=>"xsd:string"));

//Funcion obten por id
 $server->register("obten_por_id", 
  array("id" => "xsd:string","tabla" => "xsd:string"),
  array("return" => "xsd:Array"));


 $server->register("lista", 
  array("tabla" => "xsd:string"),
  array("return" => "xsd:Array"));


  @$server->service(file_get_contents("php://input"));
     

?>