<?php
require_once('./lib/class.functiones.php');
//require_once('../lib/class.functiones.php');


class modelo_cliente{

public function alta_db($registro=false, 
	 	$tabla=false){

	 	  $funcion = new Funciones();
      $link =$funcion->valida_array_registro($registro);
      
      if(!$link) {
        return false;
      }

	 	  $valores ='';
	 	  for($i=0;$i<count($registro);$i++) { 
	 	  	$valores.="'".$registro[$i]."'";
	 	  	if(($i+1)!==count($registro)){
	 	  		$valores.=",";
	 	  }

    } 

    $consulta_insercion = "INSERT INTO ".$tabla." (
    razon_social,telefono_1,telefono_2,telefono_3,rfc,calle,numero_exterior,numero_interior,colonia,codigo_postal, forma_pago,nombre_contacto,telefono_contacto,correo) VALUES(".$valores.")";

     //return $funcion->ejecuta_query($consulta_insercion,$link);

     if($funcion->ejecuta_query($consulta_insercion,$link)){
      
      return true;

     }else{

    return false;
      
     }

}


//funcion elimina
public function elimina_db($registro=False,$tabla=False){
  $funcion = new Funciones();
      $link =$funcion->valida_cadena_vacio($registro);
      
      if (!$link) {
        return false;
      }
  	
        $consulta_elimina="DELETE FROM ".$tabla." WHERE 
        id='".$registro."'";

       
   //return $funcion->ejecuta_query($consulta_elimina,$link);
    if ($funcion->ejecuta_query($consulta_elimina,$link)) {
    
        return true;
     }
     else{
      return false;
     }

}

/*
 $resultado=array('total'=>1,$tabla=>array('id'=>$registro));
     $this->array_to_xml($resultado,$this->xml_info);
        if (!empty($this->xml_info->asXML())) {
          return $this->xml_info->asXML();
        }else{
          return false;
        }

*/


//funcion modifica
public function modifica_db($registro=False,$id=False,$tabla=False){

     $funcion = new Funciones();
      $link =$funcion->valida_array_registro($registro);
      
      if(!$link) {
        return false;
      }

       $valores='';
       $campos=array('razon_social','telefono_1','telefono_2','telefono_3','rfc','calle','numero_exterior','numero_interior','colonia','codigo_postal','forma_pago','nombre_contacto','telefono_contacto','correo');

       for($i=0; $i<count($registro);$i++){ 
          $valores.=$campos[$i]."='".$registro[$i]."'";
          if ($i+1!==count($registro)) {
            $valores.=",";
          }

       }

      $consulta_modifica ="UPDATE ".$tabla." SET ".$valores." WHERE id=".$id;
   
      
      if($funcion->ejecuta_query($consulta_modifica,$link)) {
       return true;
       }else
       {
        return false;
       }
       


}

public function obten_por_id($id=False,$tabla=False){

      $funcion = new Funciones();
      $link =$funcion->valida_cadena_vacio($id);
      if(!$link){
        return false;
      }



      $consulta = "SELECT * FROM ".$tabla." WHERE id=".$id;
       $result=$link->query($consulta);
       if($link->error){

          $resultado=array('total'=>1,$tabla=>array('Error'=>'Error en la consulta'));
     $this->array_to_xml($resultado,$this->xml_info);
        return $this->xml_info->asXML();

       }

       $tabla_base=array(
           'id'=>'id',
           'razon_social'=>'razon_social',
           'telefono_1'=>'telefono_1',
           'telefono_2'=>'telefono_2',
           'telefono_3'=>'telefono_3',
           'rfc'=>'rfc',
           'calle'=>'calle',
           'numero_exterior'=>'numero_exterior',
           'numero_interior'=>'numero_interior',
           'colonia'=>'colonia',
           'codigo_postal'=>'codigo_postal',
           'forma_pago'=>'forma_pago',
           'nombre_contacto'=>'nombre_contacto',
           'telefono_contacto'=>'telefono_contacto',
           'correo'=>'correo');
    
        $resultado=array('total'=>1);
        $i=0;
        while($row=$result->fetch_array()){
          foreach($tabla_base as $campo=>$valor) {
            $resultado[$tabla][$i][$campo]=$row[$campo];
          }

            $i++;
        }


        $resultado2=array('total'=>$i);
        $resultado2+=$resultado;
        
        $this->array_to_xml($resultado2,$this->xml_info);
           if(!empty($this->xml_info)){
             return $this->xml_info->asXML();
           }else{
            return false;
        }

}


public function lista($tabla=False){

      $funcion = new Funciones();
      $link =$funcion->valida_cadena_vacio($tabla);
      if(!$link){
        return false;
      }


      $consulta = "SELECT * FROM ".$tabla;
      $result=$link->query($consulta);

         if ($link->error) {

       return false;

      }

         $tabla_base=array(
           'id'=>'id',
           'razon_social'=>'razon_social',
           'telefono_1'=>'telefono_1',
           'telefono_2'=>'telefono_2',
           'telefono_3'=>'telefono_3',
           'rfc'=>'rfc',
           'calle'=>'calle',
           'numero_exterior'=>'numero_exterior',
           'numero_interior'=>'numero_interior',
           'colonia'=>'colonia',
           'codigo_postal'=>'codigo_postal',
           'forma_pago'=>'forma_pago',
           'nombre_contacto'=>'nombre_contacto',
           'telefono_contacto'=>'telefono_contacto',
           'correo'=>'correo');


         $i=0;

         $resultado = array();

         while($row=$result->fetch_array()){
          foreach ($tabla_base as $campo => $valor) {
            $resultado[$i][$campo]=$row[$campo];
          }
          $i++;
         }

       return $resultado;

}

}


//$modelo = new modelo_grupo();
//$registro = array('registro8','comentario8','18');
//$resultado=$modelo->modifica_db($registro,47,'grupos');
//$resultado=$modelo->elimina_db(46,'grupos');
//$resultado=$modelo->alta_db($registro,'grupos');
//$resultado=$modelo->lista('grupos');
//print_r($resultado);
//header("Content-type: text/xml");
?>