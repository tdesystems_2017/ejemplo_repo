<?php
   require_once('./lib/nusoap.php');

   if(!isset($_GET['seccion'])){
     $seccion='';

   }
   else{
    $seccion = $_GET['seccion'];
      $cliente = new nusoap_client('http://localhost/ejemplo_repo/sonia/angular_crud/servidor/server_'.$seccion.'.php?wsdl','wsdl');
   }
    if(!isset($_GET['accion'])){
     $accion='';
   }
   else{
    $accion = $_GET['accion'];
   }

   if(empty($accion) || empty($seccion)){
     header("Location: http://localhost/ejemplo_repo/sonia/angular_crud");
   }

   if($accion == 'lista'){
    $resultado =$cliente->call($accion,array('tabla'=>$seccion));
     echo json_encode($resultado);
   }


   if($accion == 'alta_db'){
     $data = json_decode(file_get_contents("php://input"));
     if($seccion == 'grupo' || $seccion == 'seccion'){
        $descripcion = $data->descripcion;
        $observaciones = $data->observaciones;
        $status = $data->status;
        $array = array($descripcion,$observaciones,
          $status);

      }else if($seccion == 'accion'){
        $descripcion = $data->descripcion;
        $seccion_id = $data->seccion_id;
        $array = array($descripcion,$seccion_id);
        
      }else if($seccion == 'accion_grupo'){
        $accion_id = $data->accion_id;
        $grupo_id = $data->grupo_id;
        $array = array($accion_id,$grupo_id);

      }else if($seccion == 'personal'){
        $nombre = $data->nombre;
        $apellido_paterno = $data->apellido_paterno;
        $apellido_materno = $data->apellido_materno;
        $telefono = $data->telefono;
        $curp = $data->curp;
        $direccion = $data->direccion;
        $sueldo = $data->sueldo;
        $especialidad = $data->especialidad;
        $correo = $data->correo;

        $array = array($nombre,$apellido_paterno,$apellido_materno,$telefono,$curp,$direccion,$sueldo,$especialidad,$correo);

      }else if($seccion == 'usuario'){
        $usuario = $data->usuario;
        $contrasena = $data->contrasena;
        $personal_id = $data->personal_id;
        $grupo_id = $data->grupo_id;

        $array = array($usuario,$contrasena,$personal_id,$grupo_id);

      }else if($seccion == 'cliente'){
        $razon_social = $data->razon_social;
        $telefono_1 = $data->telefono_1;
        $telefono_2 = $data->telefono_2;
        $telefono_3 = $data->telefono_3;
        $rfc = $data->rfc;
        $calle = $data->calle;
        $numero_exterior = $data->numero_exterior;
        $numero_interior = $data->numero_interior;
        $colonia = $data->colonia;
        $codigo_postal = $data->codigo_postal;
        $forma_pago = $data->forma_pago;
        $nombre_contacto = $data->nombre_contacto;
        $telefono_contacto = $data->telefono_contacto;
        $correo = $data->correo;

        $array = array($razon_social,$telefono_1,$telefono_2,$telefono_3,$rfc,$calle,$numero_exterior,$numero_interior,$colonia, $codigo_postal,$forma_pago,$nombre_contacto,$telefono_contacto,$correo);

      }elseif ($seccion == 'proyecto'){
        $nombre = $data->nombre;
        $tiempo_estimado = $data->tiempo_estimado;
        $costo = $data->costo;
        $cliente_id = $data->cliente_id;

        $rray = array($nombre,$tiempo_estimado,$costo,$cliente_id);

      }else if($seccion == 'personal_proyecto'){
        $proyecto_id = $data->proyecto_id;
        $personal_id = $data->personal_id;

        $array = array($proyecto_id,$personal_id);
      }

      $resultado=$cliente->call($accion,array('registro'=> $array,'tabla' => $seccion)
        );

      if($resultado){
       echo true;
      }
   }

    if($accion == 'modifica_db') {
      $data = json_decode(file_get_contents("php://input"));
      $id = $data->id;
     if($seccion == 'grupo' || $seccion == 'seccion'){
        $descripcion = $data->descripcion;
        $observaciones = $data->observaciones;
        $status = $data->status;
        $array = array($descripcion,$observaciones,
          $status);

      }else if($seccion == 'accion'){
        $descripcion = $data->descripcion;
        $seccion_id = $data->seccion_id;
        $array = array($descripcion,$seccion_id);
        
      }else if($seccion == 'accion_grupo'){
        $accion_id = $data->accion_id;
        $grupo_id = $data->grupo_id;
        $array = array($accion_id,$grupo_id);

      }else if($seccion == 'personal'){
        $nombre = $data->nombre;
        $apellido_paterno = $data->apellido_paterno;
        $apellido_materno = $data->apellido_materno;
        $telefono = $data->telefono;
        $curp = $data->curp;
        $direccion = $data->direccion;
        $sueldo = $data->sueldo;
        $especialidad = $data->especialidad;
        $correo = $data->correo;

        $array = array($nombre,$apellido_paterno,$apellido_materno,$telefono,$curp,$direccion,$sueldo,$especialidad,$correo);

      }else if($seccion == 'usuario'){
        $usuario = $data->usuario;
        $contrasena = $data->contrasena;
        $personal_id = $data->personal_id;
        $grupo_id = $data->grupo_id;

        $array = array($usuario,$contrasena,$personal_id,$grupo_id);

      }else if($seccion == 'cliente'){
        $razon_social = $data->razon_social;
        $telefono_1 = $data->telefono_1;
        $telefono_2 = $data->telefono_2;
        $telefono_3 = $data->telefono_3;
        $rfc = $data->rfc;
        $calle = $data->calle;
        $numero_exterior = $data->numero_exterior;
        $numero_interior = $data->numero_interior;
        $colonia = $data->colonia;
        $codigo_postal = $data->codigo_postal;
        $forma_pago = $data->forma_pago;
        $nombre_contacto = $data->nombre_contacto;
        $telefono_contacto = $data->telefono_contacto;
        $correo = $data->correo;

        $array = array($razon_social,$telefono_1,$telefono_2,$telefono_3,$rfc,$calle,$numero_exterior,$numero_interior,$colonia, $codigo_postal,$forma_pago,$nombre_contacto,$telefono_contacto,$correo);

      }elseif ($seccion == 'proyecto'){
        $nombre = $data->nombre;
        $tiempo_estimado = $data->tiempo_estimado;
        $costo = $data->costo;
        $cliente_id = $data->cliente_id;

        $rray = array($nombre,$tiempo_estimado,$costo,$cliente_id);
      }

      elseif($seccion == 'personal_proyecto'){
        $proyecto_id = $data->proyecto_id;
        $personal_id = $data->personal_id;


        $array = array($proyecto_id,$personal_id);
      }

      $resultado=$cliente->call($accion,array('registro'=> $array,'id' => $id,'tabla' => $seccion)
        );

      if($resultado){
       echo true;
      }
    } 

 
     if($accion == 'elimina_db'){
      $data = json_decode(file_get_contents("php://input"));
      $id = $data->id;
     
      $resultado=$cliente->call($accion,array('registro'=> $id, 'tabla' => $seccion));

      if ($resultado) {
        echo true;
      }
         
     }

     //localhost/ejemplo_repo/sonia/angular_crud/servidor/cliente.php?seccion=personal_proyecto&accion=lista

//agregar a estas clases tambien
//angular-script
//cliente
?>