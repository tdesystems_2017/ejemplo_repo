<?php
require_once('./lib/class.functiones.php');
//require_once('../lib/class.functiones.php');


class modelo_grupo{

public $xml_info;
public function __construct()
{
       $this->xml_info = new SimpleXMLElement("<?xml version=\"1.0\"?><sistema></sistema>");
}


public function alta_db($registro=false, 
	 	$tabla=false){

	 	  $funcion = new Funciones();
      $link =$funcion->valida_array_registro($registro);
      
      if(!$link) {
        return false;
      }

	 	  $valores ='';
	 	  for($i=0;$i<count($registro);$i++) { 
	 	  	$valores.="'".$registro[$i]."'";
	 	  	if(($i+1)!==count($registro)){
	 	  		$valores.=",";
	 	  }

} 

$consulta_insercion = "INSERT INTO ".$tabla." (descripcion,obcervaciones,status) VALUES(".$valores.")";

     //return $funcion->ejecuta_query($consulta_insercion,$link);

     if($funcion->ejecuta_query($consulta_insercion,$link)){
      $id = $link->insert_id;
      return $this->obten_por_id($id,$tabla);

     }else{

      $resultado=array('total'=>1,$tabla=>array('Error'=>'Error en la consulta'));
     $this->array_to_xml($resultado,$this->xml_info);
        return $this->xml_info->asXML();
      
     }

}


//funcion elimina
public function elimina_db($registro=False,$tabla=False){
$funcion = new Funciones();
      $link =$funcion->valida_cadena_vacio($registro);
      
      if (!$link) {
        return false;
      }
  	
        $consulta_elimina="DELETE FROM ".$tabla." WHERE 
        id='".$registro."'";

       
   //return $funcion->ejecuta_query($consulta_elimina,$link);
    if ($funcion->ejecuta_query($consulta_elimina,$link)) {
     $resultado=array('total'=>1,$tabla=>array('id'=>$registro));
     $this->array_to_xml($resultado,$this->xml_info);
        if (!empty($this->xml_info)) {
          return $this->xml_info->asXML();
        }else{
          return false;
        }


     }else{
      return false;
     }

}

/*
 $resultado=array('total'=>1,$tabla=>array('id'=>$registro));
     $this->array_to_xml($resultado,$this->xml_info);
        if (!empty($this->xml_info->asXML())) {
          return $this->xml_info->asXML();
        }else{
          return false;
        }

*/


//funcion modifica
public function modifica_db($registro=False,$id=False,$tabla=False){

     $funcion = new Funciones();
      $link =$funcion->valida_array_registro($registro);
      
      if (!$link) {
        return false;
      }


       $valores='';
       $campos=array('descripcion','obcervaciones','status');

       for ($i=0; $i<count($registro);$i++){ 
          $valores.=$campos[$i]."='".$registro[$i]."'";
          if ($i+1!==count($registro)) {
            $valores.=",";
          }

       }

      $consulta_modifica ="UPDATE ".$tabla." SET ".$valores." WHERE id=".$id;
   
      
      if ($funcion->ejecuta_query($consulta_modifica,$link)) {
       return $this->obten_por_id($id,$tabla);
       }else
       {
        return false;
       }
       


}

public function obten_por_id($id=False,$tabla=False){

      $funcion = new Funciones();
      $link =$funcion->valida_cadena_vacio($id);
      if(!$link){
        return false;
      }



      $consulta = "SELECT * FROM ".$tabla." WHERE id=".$id;
       $result=$link->query($consulta);
       if($link->error){

          $resultado=array('total'=>1,$tabla=>array('Error'=>'Error en la consulta'));
     $this->array_to_xml($resultado,$this->xml_info);
        return $this->xml_info->asXML();

       }

       $tabla_base=array(
        'id'=>'id',
        'descripcion'=>'descripcion',
        'obcervaciones'=>'obcervaciones',
        'status'=>'status');

        $resultado=array('total'=>1);
        $i=0;
        while($row=$result->fetch_array()){
          foreach($tabla_base as $campo=>$valor) {
            $resultado[$tabla][$i][$campo]=$row[$campo];
          }

            $i++;
        }


        $resultado2=array('total'=>$i);
        $resultado2+=$resultado;
        
        $this->array_to_xml($resultado2,$this->xml_info);
           if(!empty($this->xml_info)){
             return $this->xml_info->asXML();
           }else{
            return false;
        }

}


public function lista($tabla=False){

      $funcion = new Funciones();
      $link =$funcion->valida_cadena_vacio($tabla);
      if(!$link){
        return false;
      }


      $consulta = "SELECT * FROM ".$tabla;
      $result=$link->query($consulta);

         if ($link->error) {
           
          $resultado=array('total'=>1,$tabla=>array('Error'=>'Error en la consulta'));
     $this->array_to_xml($resultado,$this->xml_info);
        return $this->xml_info->asXML();

      }

         $tabla_base=array(
          'id'=>'id',
          'descripcion'=>'descripcion',
          'obcervaciones'=>'obcervaciones',
          'status'=>'status');


         $i=0;

         while($row=$result->fetch_array()){
          foreach ($tabla_base as $campo => $valor) {
            $resultado[$tabla][$i][$campo]=$row[$campo];
          }
          $i++;
         }

  
   $listado = array('total'=>$i);
   $listado +=$resultado;
   $this->array_to_xml($listado,$this->xml_info);
     if(!empty($this->xml_info)) {
       return $this->xml_info->asXML();
     }else{
      return false;
     }

}


public function array_to_xml($array,$xml_info){
      foreach ($array as $key => $value) {
        if (is_array($value)) {
          if (!is_numeric($key)) {
            $subnodo=$xml_info->addChild("$key");
            $this->array_to_xml($value,$subnodo);
          }else{
            $subnodo=$xml_info->addChild("item$key");
            $this->array_to_xml($value,$subnodo);
          }
        }else{
          $xml_info->addChild("$key",htmlspecialchars("$value"));
        }
      }
    }
}
//$modelo = new modelo_grupo();
//$registro = array('registro8','comentario8','18');
//$resultado=$modelo->modifica_db($registro,47,'grupos');
//$resultado=$modelo->elimina_db(46,'grupos');
//$resultado=$modelo->alta_db($registro,'grupos');
//$resultado=$modelo->lista('grupos');
//print_r($resultado);
//header("Content-type: text/xml");
?>