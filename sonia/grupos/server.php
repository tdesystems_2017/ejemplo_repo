<?php

  require_once('./lib/nusoap.php');
  require_once('./controladores/controlador_grupo.php');

  $server = new nusoap_server();
  $ns = 'http://localhost/ejemplo_repo/sonia/grupos/nusoap';
  $server->configureWSDL('Grupos',$ns);
  $server->wsdl->schemaTargetNamespace=$ns;


//Funcion Alta
  $server->register("alta_db",
  	array("registro"=>"xsd:Array","tabla"=>"xsd:string"),
  	array("return"=>"xsd:Array"));
  
//Funcion Elimina
  $server->register("elimina_db", 
  array("registro"=>"xsd:string","tabla"=>"xsd:string"),
  array("return"=>"xsd:Array"));

//Funcion Modifica
  $server->register("modifica_db", 
  array("registro"=>"xsd:Array","id"=>"xsd:string","tabla"=>"xsd:string"),
  array("return"=>"xsd:Array"));

//Funcion obten por id
 $server->register("obten_por_id", 
  array("id" => "xsd:string","tabla" => "xsd:string"),
  array("return" => "xsd:Array"));


 $server->register("lista", 
  array("tabla" => "xsd:string"),
  array("return" => "xsd:Array"));


  @$server->service(file_get_contents("php://input"));
     

?>