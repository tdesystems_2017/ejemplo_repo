from flask import Flask
from flask_restful import Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json

client = MongoClient()
db = client.sistema

app = Flask(__name__)



class Seccion(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('observaciones', required=True)
        parser.add_argument('status', required=True)
        args = parser.parse_args()

        descripcion = args['descripcion']
        observaciones = args['observaciones']
        status = args['status']

        db.seccion.insert({'descripcion':descripcion,'observaciones':observaciones,'status': status})

        return {'mensaje': 'post', 'Consulta': 'registrado'}

    def get(self):

        secciones = db.seccion.find()

        seccion_enviar = []
        for seccion in secciones:
            seccion_json = {}
            seccion_json['id'] = str(seccion['_id'])
            seccion_json['descripcion'] = seccion['descripcion']
            seccion_json['observaciones'] = seccion['observaciones']
            seccion_json['status'] = seccion['status']
            seccion_enviar.append(seccion_json)

        return {'mensaje': 'get', 'Seccion': seccion_enviar}

    def put(self, seccion):
        parser = reqparse.RequestParser()
        parser.add_argument('descripcion', required=True)
        parser.add_argument('observaciones', required=True)
        parser.add_argument('status', required=True)
        args = parser.parse_args()

        descripcion = args['descripcion']
        observaciones = args['observaciones']
        status = args['status']

        db.seccion.update({'_id': ObjectId(seccion)}, {'$set': {'descripcion': descripcion,'observaciones': observaciones, 'status': status}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}



    def delete(self,seccion):
        db.seccion.remove({'_id':ObjectId(seccion)})
        return {'mensaje':'delete','seccion':seccion}




