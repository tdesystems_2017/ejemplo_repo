from flask import Flask
from flask_restful import  Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json


client = MongoClient()
db = client.sistema

app = Flask(__name__)


class PersonalProyecto(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('proyecto_id', required=True)
        parser.add_argument('personal_id', required=True)
        args = parser.parse_args()
        proyecto_id = args['proyecto_id']
        personal_id = args['personal_id']
        db.personal_proyecto.insert({'proyecto_id': proyecto_id, 'personal_id': personal_id})
        return {'mensaje': 'post', 'proyecto_id': proyecto_id, 'personal_id': personal_id}

    def get(self):
        personal_proyectos = db.personal_proyecto.find()
        personal_proyectos_enviar = []
        for personal_proyecto in personal_proyectos:
            personal_proyecto_json = {}
            personal_proyecto_json['proyecto_id'] = personal_proyecto['proyecto_id']
            personal_proyecto_json['personal_id'] = personal_proyecto['personal_id']
            personal_proyecto_json['_id'] = str(personal_proyecto['_id'])
            personal_proyectos_enviar.append(personal_proyecto_json)
        return {'mensaje': 'get', 'personal_proyecto': personal_proyectos_enviar}

    def put(self, personal_proyecto):
        parser = reqparse.RequestParser()
        parser.add_argument('proyecto_id', required=True)
        parser.add_argument('personal_id', required=True)
        args = parser.parse_args()
        descripcion = args['proyecto_id']
        observaciones = args['personal_id']
        db.personal_proyecto.update({'_id': ObjectId(personal_proyecto)}, {'$set': {'proyecto_id': descripcion, 'personal_id': observaciones}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}

    def delete(self, personal_proyecto):
        db.personal_proyecto.remove({'_id': ObjectId(personal_proyecto)})
        return {'mensaje': 'delete'}