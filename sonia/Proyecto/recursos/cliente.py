from flask import Flask
from flask_restful import Resource, reqparse
from pymongo import MongoClient
from bson.objectid import ObjectId

import json

client = MongoClient()
db = client.sistema

app = Flask(__name__)



class Cliente(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('razon_social', required=True)
        parser.add_argument('telefono_1', required=True)
        parser.add_argument('telefono_2', required=True)
        parser.add_argument('telefono_3', required=True)
        parser.add_argument('rfc', required=True)
        parser.add_argument('calle', required=True)
        parser.add_argument('numero_exterior', required=True)
        parser.add_argument('numero_interior', required=True)
        parser.add_argument('colonia', required=True)
        parser.add_argument('codigo_postal', required=True)
        parser.add_argument('forma_pago', required=True)
        parser.add_argument('nombre_contacto', required=True)
        parser.add_argument('telefono_contacto', required=True)
        parser.add_argument('correo', required=True)


        args = parser.parse_args()

        razon_social = args['razon_social']
        telefono_1 = args['telefono_1']
        telefono_2 = args['telefono_2']
        telefono_3 = args['telefono_3']
        rfc = args['rfc']
        calle = args['calle']
        numero_exterior = args['numero_exterior']
        numero_interior = args['numero_interior']
        colonia = args['colonia']
        codigo_postal = args['codigo_postal']
        forma_pago = args['forma_pago']
        nombre_contacto = args['nombre_contacto']
        telefono_contacto = args['telefono_contacto']
        correo = args['correo']


        db.cliente.insert({'razon_social': razon_social, 'telefono_1': telefono_1, 'telefono_2': telefono_2, 'telefono_3': telefono_3, 'rfc': rfc, 'calle': calle, 'numero_exterior': numero_exterior, 'numero_interior': numero_interior, 'colonia': colonia, 'codigo_postal': codigo_postal, 'forma_pago': forma_pago, 'nombre_contacto': nombre_contacto, 'telefono_contacto': telefono_contacto, 'correo': correo})

        return {'mensaje': 'post', 'Consulta': 'registrada'}

    def get(self):

        clientes = db.cliente.find()

        cliente_enviar = []
        for cliente in clientes:
            cliente_json = {}
            cliente_json['id'] = str(cliente['_id'])
            cliente_json['razon_social'] = cliente['razon_social']
            cliente_json['telefono_1'] = cliente['telefono_1']
            cliente_json['telefono_2'] = cliente['telefono_2']
            cliente_json['telefono_3'] = cliente['telefono_3']
            cliente_json['rfc'] = cliente['rfc']
            cliente_json['calle'] = cliente['calle']
            cliente_json['numero_exterior'] = cliente['numero_exterior']
            cliente_json['numero_interior'] = cliente['numero_interior']
            cliente_json['colonia'] = cliente['colonia']

            cliente_json['codigo_postal'] = cliente['codigo_postal']
            cliente_json['forma_pago'] = cliente['forma_pago']
            cliente_json['nombre_contacto'] = cliente['nombre_contacto']

            cliente_json['telefono_contacto'] = cliente['telefono_contacto']
            cliente_json['correo'] = cliente['correo']

            cliente_enviar.append(cliente_json)

        return {'mensaje': 'get', 'Cliente': cliente_enviar}

    def put(self, cliente):
        parser = reqparse.RequestParser()
        parser.add_argument('razon_social', required=True)
        parser.add_argument('telefono_1', required=True)
        parser.add_argument('telefono_2', required=True)
        parser.add_argument('telefono_3', required=True)
        parser.add_argument('rfc', required=True)
        parser.add_argument('calle', required=True)
        parser.add_argument('numero_exterior', required=True)
        parser.add_argument('numero_interior', required=True)
        parser.add_argument('colonia', required=True)
        parser.add_argument('codigo_postal', required=True)
        parser.add_argument('forma_pago', required=True)
        parser.add_argument('nombre_contacto', required=True)
        parser.add_argument('telefono_contacto', required=True)
        parser.add_argument('correo', required=True)


        args = parser.parse_args()

        razon_social = args['razon_social']
        telefono_1 = args['telefono_1']
        telefono_2 = args['telefono_2']
        telefono_3 = args['telefono_3']
        rfc = args['rfc']
        calle = args['calle']
        numero_exterior = args['numero_exterior']
        numero_interior = args['numero_interior']
        colonia = args['colonia']
        codigo_postal = args['codigo_postal']
        forma_pago = args['forma_pago']
        nombre_contacto = args['nombre_contacto']
        telefono_contacto = args['telefono_contacto']
        correo = args['correo']

        db.cliente.update({'_id': ObjectId(cliente)}, {'$set': {'razon_social': razon_social, 'telefono_1': telefono_1, 'telefono_2': telefono_2, 'telefono_3': telefono_3, 'rfc': rfc, 'calle': calle, 'numero_exterior': numero_exterior, 'numero_interior': numero_interior, 'colonia': colonia, 'codigo_postal': codigo_postal, 'forma_pago': forma_pago, 'nombre_contacto': nombre_contacto, 'telefono_contacto': telefono_contacto, 'correo': correo}})
        return {'mensaje': 'put'}

    def patch(self):
        return {'mensaje': 'patch'}



    def delete(self,cliente):
        db.cliente.remove({'_id': ObjectId(cliente)})
        return {'mensaje': 'delete', 'cliente': cliente}