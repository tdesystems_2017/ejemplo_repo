from flask import Flask
from flask_restful import Api
app = Flask(__name__)


from recursos.paises import Paises
from recursos.estados import Estados
from recursos.grupo import Grupo
from recursos.seccion import Seccion
from recursos.accion import Accion
from recursos.personal import Personal
from recursos.cliente import Cliente
from recursos.accion_grupo import AccionGrupo
from recursos.proyecto import Proyecto
from recursos.personal_proyecto import PersonalProyecto
from recursos.usuario import Usuario
from recursos.token import Token


api = Api(app)

api.add_resource(Paises, '/paises', '/paises/<string:pais>')
api.add_resource(Estados, '/estados', '/estados/<string:estado>')
api.add_resource(Grupo, '/grupo', '/grupo/<string:grupo>')
api.add_resource(Seccion, '/seccion', '/seccion/<string:seccion>')
api.add_resource(Accion, '/accion', '/accion/<string:accion>')
api.add_resource(Personal, '/personal', '/personal/<string:personal>')
api.add_resource(Cliente, '/cliente', '/cliente/<string:cliente>')
api.add_resource(AccionGrupo, '/accion_grupo', '/accion_grupo/<string:accion_grupo>')
api.add_resource(Proyecto, '/proyecto', '/proyecto/<string:proyecto>')
api.add_resource(PersonalProyecto, '/personal_proyecto', '/personal_proyecto/<string:personal_proyecto>')
api.add_resource(Usuario, '/usuario', '/usuario/<string:usuario>')
api.add_resource(Token, '/token', '/token/<string:token>')

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug= True, port=5000)